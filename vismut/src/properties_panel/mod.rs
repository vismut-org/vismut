// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;

use bevy_egui::{
    egui::{self, Ui},
    EguiContext,
};

use vismut_core::node::{NodeProperties, PropertyData};
use vismut_core::{node::Node, prelude::*};

use crate::vismut::GuiEngine;
use crate::{
    mouse_interaction::active::Active, shared::NodeAddressComponent,
    undo::prelude::UndoCommandManager, CustomStage,
};

mod bool;
mod float;
mod gray;
mod int;
mod math_operation;
mod resize_filter;
mod resize_policy;
mod rgba;
mod string;
// mod grayscale;
// mod image_path;
// mod int;
// mod math_operation;
// mod output_name;
// mod resize;

/// Contains stuff that needs to be saved between frames for `bevy_egui` to work. Since it's
/// immediate mode, we need to save some things between frames.
#[derive(Default, Resource)]
pub struct SavedProperties {
    pub changed: bool,
    properties: NodeProperties,
}

pub(crate) struct PropertiesPanelPlugin;

impl Plugin for PropertiesPanelPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(SavedProperties::default())
            .add_system_set_to_stage(
                CoreStage::Update,
                SystemSet::new()
                    .after(CustomStage::Setup)
                    .with_system(properties_menu_enter)
                    .with_system(properties_menu.after(properties_menu_enter)),
            );
    }
}

/// Stores the settings of each property.
/// This is needed to allow pressing escape while dragging a slider to cancel dragging it,
/// though this is not yet implemented.
/// It's for instance also needed to be able to edit string fields at all.
fn properties_menu_enter(
    mut saved_properties: ResMut<SavedProperties>,
    engine: Res<GuiEngine>,
    q_active_new: Query<&NodeAddressComponent, Added<Active>>,
    q_active: Query<&NodeAddressComponent, With<Active>>,
) {
    // When a node is selected, save its properties.
    if let Ok(node_address) = q_active_new.get_single() {
        saved_properties.properties = engine
            .node(**node_address)
            .expect("could not find the node")
            .properties()
            .clone();
    }

    // If the properties are marked as changed, re-apply the properties from the node.
    if saved_properties.changed {
        if let Ok(node_address) = q_active.get_single() {
            saved_properties.properties = engine
                .node(**node_address)
                .expect("could not find the node")
                .properties()
                .clone();
        }
        saved_properties.changed = false;
    }
}

/// Handles rendering the property panel.
fn properties_menu(
    mut egui_context: ResMut<EguiContext>,
    engine: Res<GuiEngine>,
    q_active: Query<&NodeAddressComponent, With<Active>>,
    mut undo_command_manager: ResMut<UndoCommandManager>,
    mut saved_properties: ResMut<SavedProperties>,
) {
    const WIDTH: f32 = 250.0;

    egui::SidePanel::right("properties")
        .resizable(false)
        .show(egui_context.ctx_mut(), |ui| {
            ui.set_max_width(WIDTH);
            ui.set_min_width(WIDTH);
            ui.set_width(WIDTH);
            if let Some(node_address) = q_active.iter().next() {
                if let Ok(node) = engine.node(**node_address) {
                    properties_gui(
                        &mut undo_command_manager,
                        ui,
                        node_address,
                        &**node,
                        &mut saved_properties,
                    );
                }
            } else {
                ui.label("Click a node to make it active");
            }
        });
}

/// Renders each property in the properties panel.
fn properties_gui(
    undo_command_manager: &mut UndoCommandManager,
    ui: &mut Ui,
    node_address: &NodeAddress,
    node: &dyn Node,
    saved_properties: &mut SavedProperties,
) {
    ui.label(node.title());
    egui::Grid::new("properties_grid")
        .num_columns(2)
        .striped(true)
        .show(ui, |ui| {
            for node_property in node.properties().properties() {
                ui.label(node_property.name());
                match node_property.data() {
                    PropertyData::Bool(value) => bool::display(
                        ui,
                        undo_command_manager,
                        *node_address,
                        *value,
                        node_property.name(),
                    ),
                    PropertyData::Float(value) => float::display(
                        ui,
                        undo_command_manager,
                        *node_address,
                        *value,
                        node_property.name(),
                    ),
                    PropertyData::Gray(value) => gray::display(
                        ui,
                        undo_command_manager,
                        *node_address,
                        *value,
                        node_property.name(),
                    ),
                    PropertyData::Int(value) => int::display(
                        ui,
                        undo_command_manager,
                        *node_address,
                        *value,
                        node_property.name(),
                    ),
                    PropertyData::MathOperation(value) => math_operation::display(
                        ui,
                        undo_command_manager,
                        *node_address,
                        *value,
                        node_property.name(),
                    ),
                    PropertyData::ResizePolicy(value) => resize_policy::display(
                        ui,
                        undo_command_manager,
                        *node_address,
                        *value,
                        node_property.name(),
                    ),
                    PropertyData::ResizeFilter(value) => resize_filter::display(
                        ui,
                        undo_command_manager,
                        *node_address,
                        *value,
                        node_property.name(),
                    ),
                    PropertyData::String(value) => string::display(
                        ui,
                        undo_command_manager,
                        *node_address,
                        value,
                        node_property.name(),
                        saved_properties,
                    ),
                    PropertyData::Rgba(value) => rgba::display(
                        ui,
                        undo_command_manager,
                        *node_address,
                        value,
                        node_property.name(),
                    ),
                }
            }
        });
}
