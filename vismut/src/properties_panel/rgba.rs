// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::core_translation::NodePropertyTranslatorRgba;

use crate::undo::prelude::*;
use bevy_egui::egui;

use bevy_egui::egui::Ui;

use vismut_core::prelude::*;

pub fn display(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    value: &[VismutPixel; 4],
    property_name: &str,
) {
    // Get the actual current value on the node.
    // let value_current = egui::Rgba::from_rgba_premultiplied(value[0], value[1], value[2], value[3]);
    let value_current = value;
    let mut value_new = *value;

    // Iterate over each component,
    // and if one of them was changed,
    // flag that we want a checkpoint.
    let mut checkpoint = false;
    for i in 0..4 {
        // Create the label.
        let label = match i {
            0 => "red",
            1 => "green",
            2 => "blue",
            _ => "alpha",
        };
        ui.end_row();
        ui.label(label);

        // Create slider.
        let response = ui.add(egui::Slider::new(&mut value_new[i], 0.0..=512.0));
        let value_current = value_current[i];

        // Update checkpoint bool.
        checkpoint = checkpoint
            || ((response.drag_released() || response.lost_focus())
                && value_new[i] != value_current);
    }

    // Add a checkpoint if we're supposed to.
    if checkpoint {
        undo_command_manager.push(Box::new(GuiUndoCommand::new(
            node_address.to_property_address(property_name),
            NodePropertyTranslatorRgba(*value_current),
            NodePropertyTranslatorRgba(value_new),
        )));
        undo_command_manager.push(Box::new(Checkpoint));
    }

    ui.end_row();
}
