// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

/// Box select tool
use crate::{
    material::HUE_AQUA,
    mouse_interaction::{
        select::{DeselectItem, SelectItem},
        Selection,
    },
    shared::NodeAddressComponent,
    undo::prelude::{Checkpoint, UndoCommandManager},
    AmbiguitySet, CustomStage, Drag, Draggable, Selected, ToolState, Workspace, CAMERA_DISTANCE,
};
use bevy::prelude::*;

#[derive(Component, Default)]
struct BoxSelect {
    start: Vec2,
    end: Vec2,
}

pub(crate) struct BoxSelectPlugin;

impl Plugin for BoxSelectPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .label(CustomStage::Update)
                .after(CustomStage::Setup)
                .with_system(
                    box_select_setup
                        .with_run_criteria(State::on_enter(ToolState::BoxSelect))
                        .ambiguous_with(AmbiguitySet),
                )
                .with_system(
                    box_select
                        .with_run_criteria(State::on_update(ToolState::BoxSelect))
                        .ambiguous_with(AmbiguitySet),
                )
                .with_system(
                    box_select_cleanup
                        .with_run_criteria(State::on_exit(ToolState::BoxSelect))
                        .ambiguous_with(AmbiguitySet),
                ),
        );
    }
}

fn box_select_setup(mut commands: Commands) {
    commands.spawn((
        SpriteBundle {
            sprite: Sprite {
                color: Color::hsla(HUE_AQUA, 0.95, 0.33, 0.3),
                ..default()
            },
            ..default()
        },
        BoxSelect::default(),
    ));
}

fn box_select(
    mut tool_state: ResMut<State<ToolState>>,
    workspace: Res<Workspace>,
    mut undo_command_manager: ResMut<UndoCommandManager>,
    mut q_box_select: Query<
        (&mut Transform, &mut Sprite, &mut BoxSelect),
        Without<NodeAddressComponent>,
    >,
    q_draggable: Query<
        (
            Option<&Selected>,
            &NodeAddressComponent,
            &GlobalTransform,
            &Sprite,
        ),
        With<Draggable>,
    >,
) {
    if let Ok((mut transform, mut sprite, mut box_select)) = q_box_select.get_single_mut() {
        if workspace.drag == Drag::Starting {
            box_select.start = workspace.cursor_world;
        }
        box_select.end = workspace.cursor_world;
        let box_box = (box_select.start, box_select.end);

        let mut hovered_node_ids = Vec::new();
        let mut to_select = Vec::new();
        let mut to_deselect = Vec::new();

        for (selected, node_address, transform, sprite) in &q_draggable {
            let Some(size) = sprite.custom_size else { continue };

            // The Y axis in the coordinate system goes up on the screen,
            // but the nodes expand downwards.
            // So we flip the Y coordinate for the size to account for this.
            let size = size * Vec2 { x: 1.0, y: -1.0 };

            let drag_box = (
                transform.translation().truncate(),
                transform.translation().truncate() + size,
            );

            if box_intersect(box_box, drag_box) {
                hovered_node_ids.push(node_address.0);

                if selected.is_none() {
                    to_select.push(node_address.0);
                }
            } else if selected.is_some() {
                to_deselect.push(node_address.0);
            }
        }

        if workspace.drag == Drag::Dropping {
            for node_address in to_deselect {
                undo_command_manager.push(Box::new(DeselectItem(Selection::Node(node_address))));
            }

            for node_address in to_select {
                undo_command_manager.push(Box::new(SelectItem(Selection::Node(node_address))));
            }

            undo_command_manager.push(Box::new(Checkpoint));

            tool_state.overwrite_replace(ToolState::None).unwrap();
        }

        // Node intersection

        // for (entity, _node_id, transform, sprite) in q_draggable.iter() {
        //     if let Some(size) = sprite.custom_size {
        //         let size_half = size / 2.0;

        //         let drag_box = (
        //             transform.translation.truncate() - size_half,
        //             transform.translation.truncate() + size_half,
        //         );

        //         if box_intersect(box_box, drag_box) {
        //             commands.entity(entity).insert(Selected);
        //         } else {
        //             commands.entity(entity).remove::<Selected>();
        //         }
        //     }
        // }

        let new_transform = Transform {
            translation: ((box_select.start + box_select.end) / 2.0).extend(CAMERA_DISTANCE),
            rotation: Quat::IDENTITY,
            scale: Vec3::ONE,
        };
        *transform = new_transform;

        sprite.custom_size = Some(box_select.start - box_select.end);
    }
}

fn interval_intersect(i_1: (f32, f32), i_2: (f32, f32)) -> bool {
    let i_1 = (i_1.0.min(i_1.1), i_1.0.max(i_1.1));
    let i_2 = (i_2.0.min(i_2.1), i_2.0.max(i_2.1));

    i_1.1 >= i_2.0 && i_2.1 >= i_1.0
}

fn box_intersect(box_1: (Vec2, Vec2), box_2: (Vec2, Vec2)) -> bool {
    let x_1 = (box_1.0.x, box_1.1.x);
    let x_2 = (box_2.0.x, box_2.1.x);
    let y_1 = (box_1.0.y, box_1.1.y);
    let y_2 = (box_2.0.y, box_2.1.y);

    interval_intersect(x_1, x_2) && interval_intersect(y_1, y_2)
}

fn box_select_cleanup(mut commands: Commands, q_box_select: Query<Entity, With<BoxSelect>>) {
    for q_box_select_e in &q_box_select {
        commands.entity(q_box_select_e).despawn();
    }
}
