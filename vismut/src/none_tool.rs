// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::{scan_code_input::ScanCodeInput, AmbiguitySet, CustomStage, ToolState};
use bevy::prelude::*;

pub(crate) struct NoneToolPlugin;

impl Plugin for NoneToolPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .label(CustomStage::Update)
                .after(CustomStage::Setup)
                .with_run_criteria(State::on_enter(ToolState::None))
                .with_system(reset_input.ambiguous_with(AmbiguitySet)),
        );
    }
}

fn reset_input(mut keyboard_input: ResMut<ScanCodeInput>) {
    keyboard_input.clear();
}
