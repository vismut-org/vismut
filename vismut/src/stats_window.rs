// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

// use std::sync::{Arc, RwLock};
//
// use bevy::prelude::*;
// use bevy_egui::{egui, EguiContext};
// use vismut_core::prelude::*;
//
// use crate::CustomStage;
//
// #[derive(Clone, Debug, Eq, Hash, PartialEq, SystemLabel)]
// enum LocalStage {
//     Event,
//     Window,
// }
//
// #[derive(Default)]
// struct PersistentData {
//     memory_count: usize,
//     memory_bytes: usize,
//     storage_count: usize,
//     storage_bytes: usize,
//     node_count: usize,
//     node_queued: usize,
//     node_processing: usize,
//     graph_count: usize,
// }
//
// pub struct ToggleStatsWindow;
// struct IsOpen(bool);
//
// pub(crate) struct StatsWindowPlugin;
// impl Plugin for StatsWindowPlugin {
//     fn build(&self, app: &mut App) {
//         app.insert_resource(IsOpen(false))
//             .insert_resource(PersistentData::default())
//             .add_event::<ToggleStatsWindow>()
//             .add_system_set_to_stage(
//                 CoreStage::Update,
//                 SystemSet::new()
//                     .after(CustomStage::Setup)
//                     .with_system(stats_window_event.label(LocalStage::Event))
//                     .with_system(
//                         stats_window
//                             .label(LocalStage::Window)
//                             .after(LocalStage::Event),
//                     ),
//             );
//     }
// }
//
// fn stats_window_event(
//     mut ev_toggle_stats_window: EventReader<ToggleStatsWindow>,
//     mut is_open: ResMut<IsOpen>,
// ) {
//     for _ in ev_toggle_stats_window.iter() {
//         is_open.0 = !is_open.0;
//     }
// }
//
// fn stats_window(
//     mut egui_context: ResMut<EguiContext>,
//     is_open: Res<IsOpen>,
//     live_graph: Res<Arc<RwLock<LiveGraph>>>,
//     tex_pro: Res<Arc<TextureProcessor>>,
//     mut persistent_data: ResMut<PersistentData>,
// ) {
//     if !is_open.0 {
//         return;
//     }
//
//     if let Ok(transient_buffer_queue) = tex_pro.transient_buffer_queue.try_read() {
//         persistent_data.memory_count = transient_buffer_queue.count_memory();
//         persistent_data.memory_bytes = transient_buffer_queue.bytes_memory() / 1_000_000;
//         persistent_data.storage_count = transient_buffer_queue.count_storage();
//         persistent_data.storage_bytes = transient_buffer_queue.bytes_storage() / 1_000_000;
//     };
//     if let Ok(live_graphs) = tex_pro.live_graph().try_read() {
//         persistent_data.graph_count = live_graphs.len();
//     }
//     if let Ok(live_graph) = live_graph.try_read() {
//         persistent_data.node_count = live_graph.node_ids().len();
//     }
//     if let Ok(count) = tex_pro.queued_node_count() {
//         persistent_data.node_queued = count;
//     }
//     if let Ok(count) = tex_pro.processing_node_count() {
//         persistent_data.node_processing = count;
//     }
//
//     egui::Window::new("stats_window").show(egui_context.ctx_mut(), |ui| {
//         egui::Grid::new("properties_grid")
//             .num_columns(2)
//             .striped(true)
//             .show(ui, |ui| {
//                 ui.label("Graph Count");
//                 ui.label(persistent_data.graph_count.to_string());
//                 ui.end_row();
//                 ui.label("Nodes");
//                 ui.end_row();
//                 ui.label("Count");
//                 ui.label(persistent_data.node_count.to_string());
//                 ui.end_row();
//                 ui.label("Queued");
//                 ui.label(persistent_data.node_queued.to_string());
//                 ui.end_row();
//                 ui.label("Processing");
//                 ui.label(persistent_data.node_processing.to_string());
//                 ui.end_row();
//                 ui.label("Buffers");
//                 ui.end_row();
//                 ui.label("In RAM");
//                 ui.label(format!(
//                     "{} ({}MB)",
//                     persistent_data.memory_count, persistent_data.memory_bytes
//                 ));
//                 ui.end_row();
//                 ui.label("In Storage");
//                 ui.label(format!(
//                     "{} ({}MB)",
//                     persistent_data.storage_count, persistent_data.storage_bytes
//                 ));
//                 ui.end_row();
//                 {}
//             });
//     });
// }
