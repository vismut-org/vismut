// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use std::fmt::Debug;

use super::{prelude::*, UndoCommandType};
use crate::{AmbiguitySet, CustomStage, ToolState};
use bevy::prelude::*;

#[derive(Debug)]
pub struct Undo;

impl UndoCommand for Undo {
    fn command_type(&self) -> super::UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        while let Some(command) = undo_command_manager.command_batch.pop_back() {
            error!("executed undo while there were unsaved commands");
            command.backward(world, undo_command_manager);
        }

        if let Some(command) = undo_command_manager.undo_stack.pop() {
            command.backward(world, undo_command_manager);
            undo_command_manager.redo_stack.push(command);
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!()
    }
}

#[derive(Debug)]
pub struct Redo;

impl UndoCommand for Redo {
    fn command_type(&self) -> super::UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        while let Some(command) = undo_command_manager.command_batch.pop_back() {
            error!("executed redo while there were unsaved commands");
            command.backward(world, undo_command_manager);
        }

        if let Some(command) = undo_command_manager.redo_stack.pop() {
            command.forward(world, undo_command_manager);
            undo_command_manager.undo_stack.push(command);
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!()
    }
}

pub struct UndoPlugin;

impl Plugin for UndoPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_to_stage(
            CoreStage::Update,
            undo.label(CustomStage::Update)
                .after(CustomStage::Setup)
                .with_run_criteria(State::on_update(ToolState::Undo))
                .ambiguous_with(AmbiguitySet),
        )
        .add_system_to_stage(
            CoreStage::Update,
            redo.label(CustomStage::Update)
                .after(CustomStage::Setup)
                .with_run_criteria(State::on_update(ToolState::Redo))
                .ambiguous_with(AmbiguitySet),
        );
    }
}

fn undo(
    mut tool_state: ResMut<State<ToolState>>,
    mut undo_command_manager: ResMut<UndoCommandManager>,
) {
    undo_command_manager.push(Box::new(Undo));
    tool_state.overwrite_replace(ToolState::None).unwrap();
}

fn redo(
    mut tool_state: ResMut<State<ToolState>>,
    mut undo_command_manager: ResMut<UndoCommandManager>,
) {
    undo_command_manager.push(Box::new(Redo));
    tool_state.overwrite_replace(ToolState::None).unwrap();
}
