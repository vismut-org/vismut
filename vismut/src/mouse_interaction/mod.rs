// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

pub mod active;
pub mod select;

use crate::menu_bar::add_node::DragToolUndo;
use crate::sync_graph::GuiEdge;
/// All workspace mouse interaction.
use crate::{
    shared::NodeAddressComponent, undo::prelude::*, AmbiguitySet, CustomStage, Drag, Dropped,
    GrabToolType, Hovered, SlotAddressSideComponent, ToolState, Workspace,
};
use bevy::prelude::*;
use vismut_core::address::NodeAddress;

use self::{
    active::{MakeItemActive, MakeNothingActive},
    select::{DeselectAll, ReplaceSelection, SelectItem, Selected},
};

pub(crate) struct MouseInteractionPlugin;

impl Plugin for MouseInteractionPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .label(CustomStage::Setup)
                .after(CustomStage::Input)
                .with_system(
                    mouse_interaction
                        .with_run_criteria(State::on_update(ToolState::None))
                        .ambiguous_with(AmbiguitySet),
                ),
        );
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Selection {
    Node(NodeAddress),
    Edge(GuiEdge),
}

/// Handles all mouse clicks and drags in the workspace. Like dragging nodes and box select.
#[allow(clippy::too_many_arguments)]
fn mouse_interaction(
    mut commands: Commands,
    mut tool_state: ResMut<State<ToolState>>,
    mut undo_command_manager: ResMut<UndoCommandManager>,
    i_mouse_button: Res<Input<MouseButton>>,
    q_hovered_node: Query<(Entity, &NodeAddressComponent), With<Hovered>>,
    q_hovered_edge: Query<(Entity, &GuiEdge), With<Hovered>>,
    q_hovered_selected_node: Query<
        Entity,
        (With<NodeAddressComponent>, With<Selected>, With<Hovered>),
    >,
    q_hovered_slot: Query<Entity, (With<SlotAddressSideComponent>, With<Hovered>)>,
    q_dropped: Query<Entity, With<Dropped>>,
    workspace: Res<Workspace>,
) {
    if workspace.cursor_hovering_egui {
        return;
    }

    let some_dropped = q_dropped.iter().count() > 0;
    for entity in &q_dropped {
        commands.entity(entity).remove::<Dropped>();
    }
    let single_click = i_mouse_button.just_released(MouseButton::Left)
        && workspace.drag != Drag::Dropping
        && !some_dropped;
    let hovered_slot = q_hovered_slot.iter().next();
    let hovered_node = q_hovered_node.iter().next();
    let hovered_edge = q_hovered_edge.iter().next();

    if single_click {
        if let Some((_, edge_id)) = hovered_edge {
            // Select the one edge
            undo_command_manager.push(Box::new(ReplaceSelection(vec![Selection::Edge(*edge_id)])));
            undo_command_manager.push(Box::new(MakeItemActive(Selection::Edge(*edge_id))));
        } else if let Some((_, node_id)) = hovered_node {
            // Select the one node
            undo_command_manager.push(Box::new(ReplaceSelection(vec![Selection::Node(node_id.0)])));
            undo_command_manager.push(Box::new(MakeItemActive(Selection::Node(node_id.0))));
        } else {
            // Deselect everything.
            undo_command_manager.push(Box::new(DeselectAll));
            undo_command_manager.push(Box::new(MakeNothingActive));
        }
        undo_command_manager.push(Box::new(Checkpoint));
    } else if workspace.drag == Drag::Starting {
        if let Some(entity) = hovered_slot {
            // Drag on slot
            commands.entity(entity).insert(Selected);
            tool_state
                .overwrite_replace(ToolState::Grab(GrabToolType::Slot))
                .unwrap();
        } else if let Some((_entity, node_address)) = hovered_node {
            // Drag on node
            let some_hovered_selected_node = q_hovered_selected_node.iter().count() > 0;
            if !some_hovered_selected_node {
                undo_command_manager.push(Box::new(DeselectAll));
                undo_command_manager.push(Box::new(SelectItem(Selection::Node(node_address.0))));
            }
            undo_command_manager.push(Box::new(DragToolUndo));
        } else {
            // Drag on empty space
            tool_state.overwrite_replace(ToolState::BoxSelect).unwrap();
        }
    }
}
