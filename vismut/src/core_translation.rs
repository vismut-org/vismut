// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use std::fmt::Debug;

use anyhow::{anyhow, Result};
use vismut_core::address::PropertyAddress;
use vismut_core::node::{Node, PropertyData, VismutFloat, VismutInt};
use vismut_core::prelude::*;

/// This is used to to provide a generic way to get and set data in the `Engine`.
///
/// If you want to get/set something on a node or in the `Engine`,
/// just implement this trait and you automatically get undo/redo support.
pub trait Translator<DataType>: Debug {
    fn get(&self, engine: &Engine) -> Result<DataType>;
    fn set(&self, engine: &mut Engine, value: DataType) -> Result<()>;
}

impl Translator<Box<dyn Node>> for NodeAddress {
    fn get(&self, engine: &Engine) -> Result<Box<dyn Node>> {
        let node = engine.node(*self)?;
        Ok(node.clone())
    }

    fn set(&self, engine: &mut Engine, value: Box<dyn Node>) -> Result<()> {
        *engine.node_mut(*self)? = value;
        Ok(())
    }
}

/// Gets and sets a bool property on a node.
#[derive(Debug, Copy, Clone)]
pub struct NodePropertyTranslatorBool(pub bool);

impl Translator<NodePropertyTranslatorBool> for PropertyAddress {
    fn get(&self, engine: &Engine) -> Result<NodePropertyTranslatorBool> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data(property_address)?;

        let PropertyData::Bool(value) = *property_data
            else { return Err(anyhow!("expected a `PropertyData::Bool`")) };
        Ok(NodePropertyTranslatorBool(value))
    }

    fn set(&self, engine: &mut Engine, value: NodePropertyTranslatorBool) -> Result<()> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data_mut(property_address)?;

        let PropertyData::Bool(current_value) = property_data
            else { return Err(anyhow!("expected a `PropertyData::Bool`")) };
        *current_value = value.0;

        Ok(())
    }
}

/// Gets and sets a float property on a node.
#[derive(Debug, Copy, Clone)]
pub struct NodePropertyTranslatorGray(pub VismutPixel);

impl Translator<NodePropertyTranslatorGray> for PropertyAddress {
    fn get(&self, engine: &Engine) -> Result<NodePropertyTranslatorGray> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data(property_address)?;

        let PropertyData::Gray(value) = *property_data
            else { return Err(anyhow!("the `PropertyData` is not a gray")) };
        Ok(NodePropertyTranslatorGray(value))
    }

    fn set(&self, engine: &mut Engine, value: NodePropertyTranslatorGray) -> Result<()> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data_mut(property_address)?;

        let PropertyData::Gray(vismut_pixel) = property_data
            else { return Err(anyhow!("the `PropertyData` is not a gray")) };
        *vismut_pixel = value.0;

        Ok(())
    }
}

/// Gets and sets a rgba property on a node.
#[derive(Debug, Copy, Clone)]
pub struct NodePropertyTranslatorRgba(pub [VismutPixel; 4]);

impl Translator<NodePropertyTranslatorRgba> for PropertyAddress {
    fn get(&self, engine: &Engine) -> Result<NodePropertyTranslatorRgba> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data(property_address)?;

        let PropertyData::Rgba(value) = *property_data
            else { return Err(anyhow!("the `PropertyData` is not an rgba")) };
        Ok(NodePropertyTranslatorRgba(value))
    }

    fn set(&self, engine: &mut Engine, value: NodePropertyTranslatorRgba) -> Result<()> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data_mut(property_address)?;

        let PropertyData::Rgba(vismut_pixels) = property_data
            else { return Err(anyhow!("the `PropertyData` is not an rgba")) };
        *vismut_pixels = value.0;

        Ok(())
    }
}

/// Gets and sets a float property on a node.
#[derive(Debug, Copy, Clone)]
pub struct NodePropertyTranslatorFloat(pub VismutFloat);

impl Translator<NodePropertyTranslatorFloat> for PropertyAddress {
    fn get(&self, engine: &Engine) -> Result<NodePropertyTranslatorFloat> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data(property_address)?;

        let PropertyData::Float(value) = *property_data
            else { return Err(anyhow!("the `PropertyData` is not a float")) };
        Ok(NodePropertyTranslatorFloat(value))
    }

    fn set(&self, engine: &mut Engine, value: NodePropertyTranslatorFloat) -> Result<()> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data_mut(property_address)?;

        let PropertyData::Float(vismut_float) = property_data
            else { return Err(anyhow!("the `PropertyData` is not a float")) };
        *vismut_float = value.0;

        Ok(())
    }
}

/// Gets and sets an integer property on a node.
#[derive(Debug, Copy, Clone)]
pub struct NodePropertyTranslatorInt(pub VismutInt);

impl Translator<NodePropertyTranslatorInt> for PropertyAddress {
    fn get(&self, engine: &Engine) -> Result<NodePropertyTranslatorInt> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data(property_address)?;

        let PropertyData::Int(value) = *property_data
            else { return Err(anyhow!("the `PropertyData` is not an int")) };
        Ok(NodePropertyTranslatorInt(value))
    }

    fn set(&self, engine: &mut Engine, value: NodePropertyTranslatorInt) -> Result<()> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data_mut(property_address)?;

        let PropertyData::Int(vismut_float) = property_data
            else { return Err(anyhow!("the `PropertyData` is not an int")) };
        *vismut_float = value.0;

        Ok(())
    }
}

/// Gets and sets a MathOperation property on a node.
#[derive(Debug, Copy, Clone)]
pub struct NodePropertyTranslatorMathOperation(pub MathOperation);

impl Translator<NodePropertyTranslatorMathOperation> for PropertyAddress {
    fn get(&self, engine: &Engine) -> Result<NodePropertyTranslatorMathOperation> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data(property_address)?;

        let PropertyData::MathOperation(value) = *property_data
            else { return Err(anyhow!("expected `PropertyData::MathOperation`")) };
        Ok(NodePropertyTranslatorMathOperation(value))
    }

    fn set(&self, engine: &mut Engine, value: NodePropertyTranslatorMathOperation) -> Result<()> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data_mut(property_address)?;

        let PropertyData::MathOperation(current_value) = property_data
            else { return Err(anyhow!("expected `PropertyData::MathOperation`")) };
        *current_value = value.0;

        Ok(())
    }
}

/// Gets and sets a ResizePolicy property on a node.
#[derive(Debug, Copy, Clone)]
pub struct NodePropertyTranslatorResizePolicy(pub ResizePolicy);

impl Translator<NodePropertyTranslatorResizePolicy> for PropertyAddress {
    fn get(&self, engine: &Engine) -> Result<NodePropertyTranslatorResizePolicy> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data(property_address)?;

        let PropertyData::ResizePolicy(value) = *property_data
            else { return Err(anyhow!("expected `PropertyData::ResizePolicy`")) };
        Ok(NodePropertyTranslatorResizePolicy(value))
    }

    fn set(&self, engine: &mut Engine, value: NodePropertyTranslatorResizePolicy) -> Result<()> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data_mut(property_address)?;

        let PropertyData::ResizePolicy(current_value) = property_data
            else { return Err(anyhow!("expected `PropertyData::ResizePolicy`")) };
        *current_value = value.0;

        Ok(())
    }
}

/// Gets and sets a ResizePolicy property on a node.
#[derive(Debug, Copy, Clone)]
pub struct NodePropertyTranslatorResizeFilter(pub ResizeFilter);

impl Translator<NodePropertyTranslatorResizeFilter> for PropertyAddress {
    fn get(&self, engine: &Engine) -> Result<NodePropertyTranslatorResizeFilter> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data(property_address)?;

        let PropertyData::ResizeFilter(value) = *property_data
            else { return Err(anyhow!("expected `PropertyData::ResizeFilter`")) };
        Ok(NodePropertyTranslatorResizeFilter(value))
    }

    fn set(&self, engine: &mut Engine, value: NodePropertyTranslatorResizeFilter) -> Result<()> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data_mut(property_address)?;

        let PropertyData::ResizeFilter(current_value) = property_data
            else { return Err(anyhow!("expected `PropertyData::ResizeFilter`")) };
        *current_value = value.0;

        Ok(())
    }
}

/// Gets and sets a string property on a node.
#[derive(Debug, Clone)]
pub struct NodePropertyTranslatorString(pub String);

impl Translator<NodePropertyTranslatorString> for PropertyAddress {
    fn get(&self, engine: &Engine) -> Result<NodePropertyTranslatorString> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data(property_address)?;

        let PropertyData::String(value) = property_data.clone()
            else { return Err(anyhow!("the `PropertyData` is not a float")) };
        Ok(NodePropertyTranslatorString(value))
    }

    fn set(&self, engine: &mut Engine, value: NodePropertyTranslatorString) -> Result<()> {
        let property_address = self
            .node_address()
            .to_property_address(self.property_name());
        let property_data = engine.node_property_data_mut(property_address)?;

        let PropertyData::String(ref mut string) = property_data
            else { return Err(anyhow!("the `PropertyData` is not a float")) };
        *string = value.0;

        Ok(())
    }
}

// /// Gets and sets the property with the given name.
// impl Translator<PropertyData> for (NodeAddress, &str) {
//     fn get(&self, engine: &Engine) -> Result<PropertyData> {
//         engine
//             .node(self.0)?
//             .property(self.1)
//             .ok_or_else(|| anyhow!("the `Node` does not have that `NodeProperty`: {}", self.1))
//             .cloned()
//     }
//
//     fn set(&self, engine: &mut Engine, value: PropertyData) -> Result<()> {
//         todo!()
//     }
// }

// impl Translator<Resize> for NodeAddress {
//     fn get(&self, engine: &Engine) -> Result<Resize> {
//         let node = engine.node(*self)?;
//         node.resize()
//             .ok_or_else(|| anyhow!("NodeType does not have a resize: {}", node))
//     }
//
//     fn set(&self, engine: &mut Engine, value: Resize) -> Result<()> {
//         let node_type = engine.node_mut(*self)?;
//         let resize = node_type
//             .resize_mut()
//             .ok_or_else(|| anyhow!("NodeType does not have a resize"))?;
//
//         *resize = value;
//
//         Ok(())
//     }
// }
//
// impl Translator<MathOperation> for NodeAddress {
//     fn get(&self, engine: &Engine) -> Result<MathOperation> {
//         let node_type = engine.node(*self)?;
//         node_type
//             .math_operation()
//             .ok_or_else(|| anyhow!("NodeType does not have a math operation: {}", node_type))
//     }
//
//     fn set(&self, engine: &mut Engine, value: MathOperation) -> Result<()> {
//         let node_type = engine.node_mut(*self)?;
//         let math_operation = node_type
//             .math_operation_mut()
//             .ok_or_else(|| anyhow!("NodeType does not have a math operation"))?;
//
//         *math_operation = value;
//
//         Ok(())
//     }
// }
//
// #[derive(Debug, Clone)]
// pub struct TranslateGrayscaleValue(pub VismutPixel);
//
// impl Translator<TranslateGrayscaleValue> for NodeAddress {
//     fn get(&self, engine: &Engine) -> Result<TranslateGrayscaleValue> {
//         let node_type = engine.node(*self)?;
//
//         if let NodeType::Grayscale(GrayscaleNode(val)) = *node_type {
//             Ok(TranslateGrayscaleValue(val))
//         } else {
//             Err(anyhow!("not a `NodeType::Grayscale: {}", node_type))
//         }
//     }
//
//     fn set(&self, engine: &mut Engine, value: TranslateGrayscaleValue) -> Result<()> {
//         let node_type = engine.node_mut(*self)?;
//         if let NodeType::Grayscale(ref mut value_mut) = node_type {
//             value_mut.0 = value.0;
//         }
//         Ok(())
//     }
// }
//
// #[derive(Debug, Clone)]
// pub struct TranslateIntValue(pub i32);
//
// impl Translator<TranslateIntValue> for NodeAddress {
//     fn get(&self, engine: &Engine) -> Result<TranslateIntValue> {
//         let node_type = engine.node(*self)?;
//
//         if let NodeType::Int(IntNode(val)) = *node_type {
//             Ok(TranslateIntValue(val))
//         } else {
//             Err(anyhow!("not a `NodeType::Int: {}", node_type))
//         }
//     }
//
//     fn set(&self, engine: &mut Engine, value: TranslateIntValue) -> Result<()> {
//         let node_type = engine.node_mut(*self)?;
//         if let NodeType::Int(ref mut value_mut) = node_type {
//             value_mut.0 = value.0;
//         }
//         Ok(())
//     }
// }
//
// #[derive(Debug, Clone)]
// pub struct TranslateFloatValue(pub f32);
//
// impl Translator<TranslateFloatValue> for NodeAddress {
//     fn get(&self, engine: &Engine) -> Result<TranslateFloatValue> {
//         let node_type = engine.node(*self)?;
//
//         if let NodeType::Float(FloatNode(val)) = *node_type {
//             Ok(TranslateFloatValue(val))
//         } else {
//             Err(anyhow!("not a `NodeType::Float: {}", node_type))
//         }
//     }
//
//     fn set(&self, engine: &mut Engine, value: TranslateFloatValue) -> Result<()> {
//         let node_type = engine.node_mut(*self)?;
//         if let NodeType::Float(ref mut value_mut) = node_type {
//             value_mut.0 = value.0;
//         }
//         Ok(())
//     }
// }
//
// #[derive(Debug, Clone)]
// pub struct TranslateName(pub String);
//
// impl Translator<TranslateName> for NodeAddress {
//     fn get(&self, engine: &Engine) -> Result<TranslateName> {
//         let name = engine.node(*self)?.name()?;
//         Ok(TranslateName(name.clone()))
//     }
//
//     fn set(&self, engine: &mut Engine, name: TranslateName) -> Result<()> {
//         engine
//             .set_name(*self, name.0)
//             .map_err(|_| anyhow!("wrong node type"))
//     }
// }
