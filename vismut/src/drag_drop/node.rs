// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use std::fmt::Debug;
use std::num::NonZeroU8;

use crate::grid::{GridGhostOrigin, GridGhostSprite, GridGhostSpriteBundle};
use crate::shared::{SnapToGrid, NODE_HEIGHT_GRID, NODE_WIDTH_GRID};
use crate::undo::undo_command_manager::UndoCancel;
use crate::{
    shared::NodeAddressComponent, undo::prelude::*, BackplateBundle, Cursor, DragDropMode,
    GridPosition, GridSize, Selected, ToolState,
};
use bevy::prelude::*;
use bevy::sprite::Anchor;

use vismut_core::prelude::*;

use super::{Dragged, Dropped};

#[derive(Resource)]
pub struct MoveToCursor(pub bool);

#[derive(Clone, Debug)]
pub struct MoveNodeUndo {
    pub node_address: NodeAddress,
    pub from: GridPosition,
    pub to: GridPosition,
}

impl UndoCommand for MoveNodeUndo {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut query = world.query::<(Entity, &NodeAddressComponent, &mut GridPosition)>();
        if let Some((entity, _, mut grid_position)) = query
            .iter_mut(world)
            .find(|(_, node_id, _)| node_id.0 == self.node_address)
        {
            grid_position.x = self.to.x;
            grid_position.y = self.to.y;
            world.entity_mut(entity).insert(SnapToGrid);
        }
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut query = world.query::<(Entity, &NodeAddressComponent, &mut GridPosition)>();
        if let Some((entity, _, mut grid_position)) = query
            .iter_mut(world)
            .find(|(_, node_id, _)| node_id.0 == self.node_address)
        {
            grid_position.x = self.from.x;
            grid_position.y = self.from.y;
            world.entity_mut(entity).insert(SnapToGrid);
        }
    }
}

/// Grab all selected nodes.
pub(crate) fn grab_node_setup(
    mut commands: Commands,
    mut move_to_cursor: ResMut<MoveToCursor>,
    mut tool_state: ResMut<State<ToolState>>,
    mut q_selected_nodes: Query<
        (Entity, &mut Transform, &GlobalTransform),
        (With<NodeAddressComponent>, With<Selected>),
    >,
    q_cursor: Query<(Entity, &GlobalTransform), With<Cursor>>,
) {
    let (cursor_e, cursor_transform) = q_cursor.single();
    let mut any_nodes = false;

    for (index, (entity, mut transform, global_transform)) in
        q_selected_nodes.iter_mut().enumerate()
    {
        commands.entity(cursor_e).push_children(&[entity]);
        commands
            .entity(entity)
            .insert(Dragged {
                start: global_transform.translation().truncate(),
            })
            .insert(GridGhostOrigin);

        commands.spawn(GridGhostSpriteBundle {
            grid_ghost_sprite: GridGhostSprite {
                origin_entity: entity,
                overlapping: true,
            },
            backplate_bundle: BackplateBundle {
                sprite_bundle: SpriteBundle {
                    sprite: Sprite {
                        custom_size: Some(Vec2::default()),
                        anchor: Anchor::TopLeft,
                        ..default()
                    },
                    ..default()
                },
                ..default()
            },
            snap_to_grid: Default::default(),
        });

        if move_to_cursor.0 {
            let height = GridSize::new(
                NonZeroU8::new(NODE_WIDTH_GRID as u8).unwrap(),
                NonZeroU8::new(NODE_HEIGHT_GRID as u8).unwrap(),
            )
            .to_pixels()
            .1;

            let width_offset = -height / 2.0;

            transform.translation.x = 0.0;
            transform.translation.y = width_offset * index as f32;
        } else {
            let cursor_space = global_transform.translation() - cursor_transform.translation();
            transform.translation.x = cursor_space.x;
            transform.translation.y = cursor_space.y;
        }

        any_nodes = true;
    }

    if move_to_cursor.0 {
        move_to_cursor.0 = false;
    }

    if !any_nodes {
        tool_state.overwrite_replace(ToolState::None).unwrap();
    }
}

/// Exit grab tool if mouse button is released.
pub(crate) fn grab_node_update(
    mut commands: Commands,
    mut undo_command_manager: ResMut<UndoCommandManager>,
    q_dragged: Query<(Entity, &NodeAddressComponent, &Dragged, &GlobalTransform)>,
    q_ghosts: Query<&GridGhostSprite>,
    mut tool_state: ResMut<State<ToolState>>,
    mut i_mouse_button: ResMut<Input<MouseButton>>,
    mut drag_drop_mode: ResMut<DragDropMode>,
) {
    // This stops an `Image` node from being immediately dropped when created by double-clicking on
    // an image in the file browser.
    if let DragDropMode::Pressed = *drag_drop_mode {
        if i_mouse_button.just_pressed(MouseButton::Left) {
            *drag_drop_mode = DragDropMode::Released;
        } else {
            return;
        }
    }

    if i_mouse_button.just_released(MouseButton::Left) {
        let blocked = q_ghosts
            .iter()
            .any(|grid_ghost_sprite| grid_ghost_sprite.overlapping);

        for (entity, node_id, dragged, gtransform) in &q_dragged {
            if blocked {
                undo_command_manager.push(Box::new(UndoCancel));
            } else {
                let from = GridPosition::from_translation(dragged.start);
                let to = GridPosition::from_translation(gtransform.translation().truncate());

                if from != to {
                    undo_command_manager.push(Box::new(MoveNodeUndo {
                        node_address: node_id.0,
                        from,
                        to,
                    }));
                }
            }

            commands
                .entity(entity)
                .remove::<Parent>()
                .insert(Dropped)
                .insert(SnapToGrid);
        }

        undo_command_manager.push(Box::new(Checkpoint));
        tool_state.overwrite_replace(ToolState::None).unwrap();

        i_mouse_button.clear();
    }
}

pub(crate) fn grab_node_cleanup(
    mut commands: Commands,
    mut q_dragged: Query<Entity, With<NodeAddressComponent>>,
    q_cursor: Query<Entity, With<Cursor>>,
) {
    let cursor_entity = q_cursor.single();

    for dragged_entity in &mut q_dragged {
        commands
            .entity(dragged_entity)
            .remove::<Dragged>()
            .remove::<GridGhostOrigin>();

        commands
            .entity(cursor_entity)
            .remove_children(&[dragged_entity]);
    }
}
