use crate::material::HUE_RED;
use crate::shared::SnapToGrid;
use crate::{BackplateBundle, Dragged, GridPosition, GridSize};
use bevy::prelude::*;

#[derive(Component, Default)]
pub struct GridGhostOrigin;

#[derive(Bundle, Default)]
pub(super) struct GridGhostOriginBundle {
    grid_ghost_origin: GridGhostOrigin,
    #[bundle]
    transform: SpatialBundle,
    grid_size: GridSize,
}

#[derive(Component)]
pub struct GridGhostSprite {
    pub origin_entity: Entity,
    pub overlapping: bool,
}

impl GridGhostSprite {
    pub fn origin_entity(&self) -> Entity {
        self.origin_entity
    }
}

#[derive(Bundle)]
pub struct GridGhostSpriteBundle {
    pub grid_ghost_sprite: GridGhostSprite,
    #[bundle]
    pub backplate_bundle: BackplateBundle,
    pub snap_to_grid: SnapToGrid,
}

/// Checks if a pair of `GridPosition`s and `GridSizes` overlap.
pub fn grid_overlaps(
    a_grid_pos: GridPosition,
    a_grid_size: GridSize,
    b_grid_pos: GridPosition,
    b_grid_size: GridSize,
) -> bool {
    let a_left = a_grid_pos.x;
    let a_bottom = a_grid_pos.y - a_grid_size.height.get() as i32 + 1;
    let a_right = a_grid_pos.x + a_grid_size.width.get() as i32 - 1;
    let a_top = a_grid_pos.y;

    let b_left = b_grid_pos.x;
    let b_bottom = b_grid_pos.y - b_grid_size.height.get() as i32 + 1;
    let b_right = b_grid_pos.x + b_grid_size.width.get() as i32 - 1;
    let b_top = b_grid_pos.y;

    // Check for overlapping with all things on the grid.
    // This is a bit confusing because in the grid,
    // X points to the right,
    // and Y points up,
    // but the bs extend down and right.
    // So Y is flipped.
    b_left <= a_right && b_right >= a_left && b_bottom <= a_top && b_top >= a_bottom
}

/// Updates the color of all ghost images based on if they're overlapping or not.
pub(super) fn ghost_color(
    mut q_ghost: Query<(&GridGhostSprite, &mut Sprite), Changed<GridGhostSprite>>,
) {
    for (grid_ghost_sprite, mut sprite) in &mut q_ghost {
        let color = if grid_ghost_sprite.overlapping {
            Color::hsl(HUE_RED, 0.7, 0.3)
        } else {
            Color::NONE
        };

        sprite.color = color;
    }
}

/// Updates the overlapping state of all ghost images.
pub(super) fn ghost_overlapping(
    mut q_ghost: Query<
        (&GridSize, &GridPosition, &mut GridGhostSprite),
        (Or<(Changed<GridPosition>, Changed<GridSize>)>,),
    >,
    q_node: Query<(&GridPosition, &GridSize), (Without<Dragged>, Without<GridGhostSprite>)>,
) {
    for (ghost_grid_size, ghost_grid_position, mut grid_ghost_sprite) in &mut q_ghost {
        grid_ghost_sprite.overlapping = false;

        for (node_grid_position, node_grid_size) in &q_node {
            if grid_overlaps(
                *ghost_grid_position,
                *ghost_grid_size,
                *node_grid_position,
                *node_grid_size,
            ) {
                grid_ghost_sprite.overlapping = true;
                break;
            }
        }
    }
}

/// Updates the `GridGhostSprite`s based on their origin entities.
pub(super) fn ghost_sprite_update(
    mut commands: Commands,
    q_grid_ghost_origin: Query<
        (Entity, &GlobalTransform, &GridSize),
        (With<GridGhostOrigin>, Without<GridGhostSprite>),
    >,
    mut q_grid_ghost_sprite: Query<(Entity, &GridGhostSprite, &mut GridPosition, &mut GridSize)>,
) {
    for (sprite_entity, grid_ghost_sprite, mut sprite_grid_position, mut sprite_grid_size) in
        &mut q_grid_ghost_sprite
    {
        let mut found_origin = false;
        let target_origin_entity = grid_ghost_sprite.origin_entity();

        for (origin_entity, origin_global_transform, origin_grid_size) in &q_grid_ghost_origin {
            if origin_entity == target_origin_entity {
                *sprite_grid_position = GridPosition::from_translation(
                    origin_global_transform.translation().truncate(),
                );
                *sprite_grid_size = *origin_grid_size;
                commands.entity(sprite_entity).insert(SnapToGrid);
                found_origin = true;
                break;
            }
        }

        if !found_origin {
            commands.entity(sprite_entity).despawn_recursive();
        }
    }
}
