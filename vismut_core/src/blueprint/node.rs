use serde::{Deserialize, Serialize};

use crate::address::NodeId;
use crate::node::Node;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct DagNode {
    #[serde(rename = "id")]
    pub node_id: NodeId,
    pub node: Box<dyn Node>,
}

impl DagNode {
    pub const fn new(node_id: NodeId, node: Box<dyn Node>) -> Self {
        Self { node_id, node }
    }
}
