mod activate;

use crate::address::{NodeAddress, Side, SlotAddress};
use crate::blueprint::dag::BlueprintDag;
use crate::engine::{InterDagEdge, NodeState};
use crate::live::address::LiveNodeId;
use crate::live::dag::LiveDag;
use crate::live::edge::LiveEdge;
// use crate::node::NodeType;
use crate::node::Node;
// use crate::FloatNode;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct FlatEdge {
    pub output: SlotAddress,
    pub input: SlotAddress,
}

impl From<InterDagEdge> for FlatEdge {
    fn from(inter_dag_edge: InterDagEdge) -> Self {
        Self {
            output: inter_dag_edge.output,
            input: inter_dag_edge.input,
        }
    }
}

#[derive(Clone, Debug)]
struct FlatNode {
    pub node_address: NodeAddress,
    pub node: Box<dyn Node>,
}

/// A flattened representation of a `&[BlueprintDag]`. Has turned the nested structures into flat
/// `Vec`s that uses more specific addresses to refer to specific nodes and edges, instead of using
/// the structure. This flattened representation simplifies finding changes for partial `LiveDag`
/// updates.
#[derive(Debug)]
pub struct FlatBlueprint {
    nodes: Vec<FlatNode>,
    edges: Vec<FlatEdge>,
}

impl FlatBlueprint {
    pub fn new() -> Self {
        Self {
            nodes: Vec::new(),
            edges: Vec::new(),
        }
    }
}

impl LiveDag {
    /// Sets up the dag to make it ready to be executed. This needs to be called before calling
    /// `run()` for any changes to make a difference.
    pub(crate) fn prepare(
        &mut self,
        blueprint_dags: &[BlueprintDag],
        inter_dag_edges: &[InterDagEdge],
    ) {
        self.update_dag_properties(blueprint_dags);

        let fresh_flat_dag = Self::flatten_blueprint_dags(blueprint_dags, inter_dag_edges);

        self.synchronize_nodes(&fresh_flat_dag);
        self.synchronize_edges(&fresh_flat_dag.edges);
        self.propagate_dirt();

        self.blueprint_dags_compare = fresh_flat_dag;
    }

    /// Updates the collection of available `DagProperties`.
    fn update_dag_properties(&mut self, blueprint_dags: &[BlueprintDag]) {
        self.dag_properties.clear();
        for blueprint_dag in blueprint_dags {
            self.dag_properties.insert(
                blueprint_dag.dag_id,
                blueprint_dag.dag.dag_properties.clone(),
            );
        }
    }

    /// Propagates any `NodeState::Dirty` to child nodes.
    fn propagate_dirt(&mut self) {
        let dirty_node_ids = self.live_node_ids_in_state(NodeState::Dirty);

        for live_node_id in dirty_node_ids {
            self.dirty_recursive(live_node_id);
        }
    }

    /// Recursively make nodes dirty.
    fn dirty_recursive(&mut self, live_node_id: LiveNodeId) {
        let mut child_nodes = self.child_nodes(live_node_id).collect::<Vec<_>>();
        child_nodes.sort_unstable();
        child_nodes.dedup();

        for live_node_id in child_nodes {
            if *self
                .node_state(live_node_id)
                .expect("we just got this `NodeId`, so it should exist")
                != NodeState::Dirty
            {
                self.invalidate_node(live_node_id)
                    .expect("the node should exist");
                self.dirty_recursive(live_node_id);
            }
        }
    }

    /// Returns all `LiveNodeId`s connected to the output side of the given `LiveNodeId`. The output
    /// is unsorted and may contain duplicates.
    fn child_nodes(&self, live_node_id: LiveNodeId) -> impl Iterator<Item = LiveNodeId> + '_ {
        let output_edges = self.output_edges(live_node_id);
        output_edges.map(|live_edge| live_edge.input.live_node_id)
    }

    /// Returns all `LiveEdge`s on the output side of the given `LiveNodeId`.
    fn output_edges(&self, live_node_id: LiveNodeId) -> impl Iterator<Item = &LiveEdge> {
        self.live_edges
            .iter()
            .filter(move |live_edge| live_edge.output.live_node_id == live_node_id)
    }

    /// Takes a collection of `BlueprintDag`s and flattens them to produce a `FlatBlueprint`.
    fn flatten_blueprint_dags(
        blueprint_dags: &[BlueprintDag],
        inter_dag_edges: &[InterDagEdge],
    ) -> FlatBlueprint {
        let mut flat_blueprint = FlatBlueprint {
            nodes: Vec::new(),
            edges: Vec::new(),
        };

        for dag in blueprint_dags {
            for node in &dag.dag.dag_nodes {
                let node_address = dag.dag_id.with_node_id(node.node_id);
                let node = node.node.clone();
                let flat_node = FlatNode { node_address, node };
                flat_blueprint.nodes.push(flat_node);
            }

            for edge in &dag.dag.dag_edges {
                let input = dag
                    .dag_id
                    .with_node_id(edge.node_id_input)
                    .with_slot_id(edge.slot_id_input);
                let output = dag
                    .dag_id
                    .with_node_id(edge.node_id_output)
                    .with_slot_id(edge.slot_id_output);
                flat_blueprint.edges.push(FlatEdge { output, input });
            }
        }

        let inter_dag_edges = inter_dag_edges.iter().map(|edge| FlatEdge::from(*edge));
        flat_blueprint.edges.extend(inter_dag_edges);

        flat_blueprint
    }

    /// Finds any differences between the given `fresh_flat_nodes` and the saved `FlatNode`s.
    /// `LiveNode`s are added or removed to synchronize the state.
    fn synchronize_nodes(&mut self, fresh_flat_dag: &FlatBlueprint) {
        let (added_nodes, removed_nodes, restore_live_edges) = {
            let mut added_nodes: Vec<FlatNode> = Vec::new();
            let mut removed_nodes: Vec<NodeAddress> = Vec::new();
            let mut restore_live_edges: Vec<NodeAddress> = Vec::new();
            let mut visited_indices = Vec::new();
            let saved_nodes = &self.blueprint_dags_compare.nodes;

            for bp_node in &fresh_flat_dag.nodes {
                if let Some((index, saved_node)) = saved_nodes
                    .iter()
                    .enumerate()
                    .find(|(_, saved_node)| saved_node.node_address == bp_node.node_address)
                {
                    visited_indices.push(index);
                    if *saved_node.node != *bp_node.node {
                        // It exists, but has been changed, remove it and then add it.
                        removed_nodes.push(bp_node.node_address);
                        added_nodes.push(bp_node.clone());
                        restore_live_edges.push(bp_node.node_address);
                    }
                } else {
                    // It is new, add it.
                    added_nodes.push(bp_node.clone());
                }
            }

            for (index, saved_node) in saved_nodes.iter().enumerate() {
                if !visited_indices.contains(&index) {
                    // The node was not found when going through all current flat nodes, that means
                    // it has been removed, so add it to list for removal.
                    removed_nodes.push(saved_node.node_address);
                }
            }

            (added_nodes, removed_nodes, restore_live_edges)
        };

        for removed_node in removed_nodes {
            self.remove_live_nodes(removed_node);
        }
        for added_node in added_nodes {
            self.activate_node(added_node);
        }
        for restore_edges_node in restore_live_edges {
            self.restore_edges(restore_edges_node, &fresh_flat_dag.edges);
        }
    }

    /// Takes a `Node` and turns it into zero or more `LiveNode`s and `LiveEdge`s. Also creates slot
    /// mappings between the blueprint world and the live world.
    fn activate_node(&mut self, flat_node: FlatNode) {
        let creator_address = flat_node.node_address;
        flat_node.node.activate(self, creator_address)
    }

    /// Finds any differences between the given `fresh_flat_edges` and the saved `FlatEdge`s.
    /// `LiveEdge`s are added or removed to synchronize the state.
    fn synchronize_edges(&mut self, fresh_flat_edges: &[FlatEdge]) {
        let (added_edges, removed_edges) = {
            let mut added_edges = Vec::new();
            let mut visited_indices = Vec::new();
            let saved_edges = &self.blueprint_dags_compare.edges;

            for fresh_edge in fresh_flat_edges {
                if let Some((index, _)) = saved_edges
                    .iter()
                    .enumerate()
                    .find(|(_, edge)| *edge == fresh_edge)
                {
                    visited_indices.push(index);
                } else {
                    added_edges.push(fresh_edge);
                }
            }

            let mut removed_edges = Vec::new();
            for (index, saved_edge) in saved_edges.iter().enumerate() {
                if !visited_indices.contains(&index) {
                    removed_edges.push(*saved_edge);
                }
            }

            (added_edges, removed_edges)
        };

        for removed_edge in removed_edges {
            let input = self
                .slot_address_map
                .get(&removed_edge.input.with_side(Side::Input))
                .cloned();
            if let Some(input) = input {
                // Invalidate any nodes that used the edge as input.
                let _ = self.invalidate_node(input.live_node_id);

                let output = self
                    .slot_address_map
                    .get(&removed_edge.output.with_side(Side::Output))
                    .cloned();
                if let Some(output) = output {
                    // If the edge can be found in the mappings, remove its corresponding
                    // `LiveEdge`.

                    let live_edge = LiveEdge {
                        output: output.without_side(),
                        input: input.without_side(),
                    };

                    let index = self
                        .live_edges
                        .iter()
                        .position(|saved_edge| *saved_edge == live_edge);
                    if let Some(index) = index {
                        self.live_edges.swap_remove(index);
                    }
                }
            }
        }

        for added_edge in added_edges {
            let output = self
                .slot_address_map
                .get(&added_edge.output.with_side(Side::Output))
                .expect("the slot should exist")
                .without_side();
            let input = self
                .slot_address_map
                .get(&added_edge.input.with_side(Side::Input))
                .unwrap_or_else(|| panic!("the input slot `{}` should exist", added_edge.input))
                .without_side();
            let live_edge = LiveEdge { output, input };

            self.invalidate_node(input.live_node_id)
                .expect("node should always exist");

            self.live_edges.push(live_edge);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::address::{DagId, NodeId, SlotAddressSide, SlotId};

    use crate::live::address::{LiveNodeId, LiveSlotAddress, LiveSlotAddressSide};
    // use crate::prelude::{GrayscaleNode, MergeRgbaNode, Resize};

    use std::collections::BTreeMap;

    // todo: reactivate and rewrite test when traitification is done.
    // #[test]
    // fn flatten_blueprint_dags() {
    //     let dag = Dag {
    //         node_types: vec![
    //             BlueprintNode {
    //                 node_id: NodeId(0),
    //                 node: NodeType::Grayscale(GrayscaleNode(0.5)),
    //             },
    //             BlueprintNode {
    //                 node_id: NodeId(1),
    //                 node: NodeType::MergeRgba(MergeRgbaNode(Resize::default())),
    //             },
    //         ],
    //         edges: vec![DagEdge {
    //             node_id_output: NodeId(0),
    //             slot_id_output: SlotId(0),
    //             node_id_input: NodeId(1),
    //             slot_id_input: SlotId(0),
    //         }],
    //         node_counter: 2,
    //     };
    //
    //     let blueprint_dags = vec![
    //         BlueprintDag {
    //             dag_id: DagId(0),
    //             dag: dag.clone(),
    //             one_shot: false,
    //         },
    //         BlueprintDag {
    //             dag_id: DagId(1),
    //             dag,
    //             one_shot: false,
    //         },
    //     ];
    //
    //     let flat_blueprint = LiveDag::flatten_blueprint_dags(&blueprint_dags, &[]);
    //
    //     let expected_flat_blueprint = FlatBlueprint {
    //         nodes: vec![
    //             FlatNode {
    //                 node_address: NodeAddress {
    //                     dag_id: DagId(0),
    //                     node_id: NodeId(0),
    //                 },
    //                 node_type: NodeType::Grayscale(GrayscaleNode(0.5)),
    //             },
    //             FlatNode {
    //                 node_address: NodeAddress {
    //                     dag_id: DagId(0),
    //                     node_id: NodeId(1),
    //                 },
    //                 node_type: NodeType::MergeRgba(MergeRgbaNode(Resize::default())),
    //             },
    //             FlatNode {
    //                 node_address: NodeAddress {
    //                     dag_id: DagId(1),
    //                     node_id: NodeId(0),
    //                 },
    //                 node_type: NodeType::Grayscale(GrayscaleNode(0.5)),
    //             },
    //             FlatNode {
    //                 node_address: NodeAddress {
    //                     dag_id: DagId(1),
    //                     node_id: NodeId(1),
    //                 },
    //                 node_type: NodeType::MergeRgba(MergeRgbaNode(Resize::default())),
    //             },
    //         ],
    //         edges: vec![
    //             FlatEdge {
    //                 output: SlotAddress {
    //                     dag_id: DagId(0),
    //                     node_id: NodeId(0),
    //                     slot_id: SlotId(0),
    //                 },
    //                 input: SlotAddress {
    //                     dag_id: DagId(0),
    //                     node_id: NodeId(1),
    //                     slot_id: SlotId(0),
    //                 },
    //             },
    //             FlatEdge {
    //                 output: SlotAddress {
    //                     dag_id: DagId(1),
    //                     node_id: NodeId(0),
    //                     slot_id: SlotId(0),
    //                 },
    //                 input: SlotAddress {
    //                     dag_id: DagId(1),
    //                     node_id: NodeId(1),
    //                     slot_id: SlotId(0),
    //                 },
    //             },
    //         ],
    //     };
    //
    //     assert_eq!(flat_blueprint, expected_flat_blueprint);
    // }

    fn new_slot_address_side(
        dag_id: DagId,
        node_id: NodeId,
        side: Side,
        slot_id: SlotId,
    ) -> SlotAddressSide {
        SlotAddressSide {
            dag_id,
            node_id,
            side,
            slot_id,
        }
    }

    fn new_live_slot_address_side(
        live_node_id: LiveNodeId,
        side: Side,
        slot_id: SlotId,
    ) -> LiveSlotAddressSide {
        LiveSlotAddressSide {
            live_node_id,
            side,
            slot_id,
        }
    }

    #[test]
    fn synchronize_edges() {
        // Setup
        let mut slot_address_map = BTreeMap::new();
        slot_address_map.insert(
            new_slot_address_side(DagId(0), NodeId(0), Side::Output, SlotId(0)),
            new_live_slot_address_side(LiveNodeId(0), Side::Output, SlotId(0)),
        );
        slot_address_map.insert(
            new_slot_address_side(DagId(0), NodeId(1), Side::Input, SlotId(0)),
            new_live_slot_address_side(LiveNodeId(1), Side::Input, SlotId(0)),
        );
        slot_address_map.insert(
            new_slot_address_side(DagId(1), NodeId(1), Side::Output, SlotId(0)),
            new_live_slot_address_side(LiveNodeId(2), Side::Output, SlotId(0)),
        );
        slot_address_map.insert(
            new_slot_address_side(DagId(1), NodeId(2), Side::Input, SlotId(1)),
            new_live_slot_address_side(LiveNodeId(3), Side::Input, SlotId(1)),
        );

        let mut live_dag = LiveDag::new();
        live_dag.slot_address_map = slot_address_map.clone();

        let flat_edges = vec![
            FlatEdge {
                output: SlotAddress {
                    dag_id: DagId(0),
                    node_id: NodeId(0),
                    slot_id: SlotId(0),
                },
                input: SlotAddress {
                    dag_id: DagId(0),
                    node_id: NodeId(1),
                    slot_id: SlotId(0),
                },
            },
            FlatEdge {
                output: SlotAddress {
                    dag_id: DagId(1),
                    node_id: NodeId(1),
                    slot_id: SlotId(0),
                },
                input: SlotAddress {
                    dag_id: DagId(1),
                    node_id: NodeId(2),
                    slot_id: SlotId(1),
                },
            },
        ];

        live_dag.node_states = vec![
            (LiveNodeId(0), NodeState::Dirty),
            (LiveNodeId(1), NodeState::Dirty),
            (LiveNodeId(2), NodeState::Dirty),
            (LiveNodeId(3), NodeState::Dirty),
        ];

        // Run the function
        live_dag.synchronize_edges(&flat_edges);

        // Check against expectations
        let live_edges_expected = vec![
            LiveEdge {
                output: LiveSlotAddress {
                    live_node_id: LiveNodeId(0),
                    slot_id: SlotId(0),
                },
                input: LiveSlotAddress {
                    live_node_id: LiveNodeId(1),
                    slot_id: SlotId(0),
                },
            },
            LiveEdge {
                output: LiveSlotAddress {
                    live_node_id: LiveNodeId(2),
                    slot_id: SlotId(0),
                },
                input: LiveSlotAddress {
                    live_node_id: LiveNodeId(3),
                    slot_id: SlotId(1),
                },
            },
        ];

        assert_eq!(live_dag.live_edges, live_edges_expected);
    }
}
