use super::prelude::*;

use crate::live::dag::LiveDag;
use crate::node::private::InterfaceNodePrivate;
use crate::node::resize_private::NodeResizePrivate;
use crate::node::{Node, NodeProperties, NodeProperty, PropertyData};
use crate::prelude::*;
use crate::resize::ResizeFilter;
use serde::{Deserialize, Serialize};
use std::string::ToString;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeResize {
    properties: NodeProperties,
}

const RESIZE_POLICY: &str = "resize_policy";
const RESIZE_FILTER: &str = "resize_filter";
const INPUT_A: &str = "input_a";
const INPUT_B: &str = "input_b";

#[typetag::serde]
impl Node for NodeResize {
    fn new_unboxed() -> Self {
        NodeResize {
            properties: NodeProperties::new(vec![
                NodeProperty::new(
                    RESIZE_POLICY,
                    PropertyData::ResizePolicy(ResizePolicy::default()),
                ),
                NodeProperty::new(
                    RESIZE_FILTER,
                    PropertyData::ResizeFilter(ResizeFilter::default()),
                ),
                NodeProperty::new(INPUT_A, PropertyData::Gray(0.0)),
                NodeProperty::new(INPUT_B, PropertyData::Gray(0.0)),
            ]),
        }
    }

    /// The output slots just return the input slots,
    /// because each input buffer should have a corresponding output.
    fn slots_output(&self) -> Vec<Slot> {
        self.slots_input()
    }

    fn title(&self) -> String {
        "Resize".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeResize {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        // Create the resize node.
        let mut node_resize = NodeResizePrivate::new();

        // Configure the resize node.
        let resize_policy = self
            .properties()
            .get(RESIZE_POLICY)
            .expect("expected a `PropertyData::ResizePolicy`")
            .clone();
        *node_resize
            .properties_mut()
            .get_mut(RESIZE_POLICY)
            .expect("expected a `PropertyData::ResizePolicy`") = resize_policy;

        let resize_filter = self
            .properties()
            .get(RESIZE_FILTER)
            .expect("expected a `PropertyData::ResizeFilter`")
            .clone();
        *node_resize
            .properties_mut()
            .get_mut(RESIZE_FILTER)
            .expect("expected a `PropertyData::ResizeFilter`") = resize_filter;

        node_resize.add_resize_slot(self, INPUT_A).unwrap();
        node_resize.add_resize_slot(self, INPUT_B).unwrap();

        // Insert the resize node.
        let resize_node_id = live_dag.insert(creator_address, node_resize);

        // Map inputs.
        live_dag.map_corresponding_name(
            creator_address,
            resize_node_id,
            Side::Input,
            self,
            INPUT_A,
        );
        live_dag.map_corresponding_name(
            creator_address,
            resize_node_id,
            Side::Input,
            self,
            INPUT_B,
        );

        // Map outputs.
        live_dag.map_corresponding_name(
            creator_address,
            resize_node_id,
            Side::Output,
            self,
            INPUT_A,
        );
        live_dag.map_corresponding_name(
            creator_address,
            resize_node_id,
            Side::Output,
            self,
            INPUT_B,
        );
    }
}
