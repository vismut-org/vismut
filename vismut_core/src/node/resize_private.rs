use super::prelude::*;

use crate::error::{Result, VismutError};
use crate::live::address::LiveNodeId;
use crate::live::dag::LiveDag;
use crate::live::edge::LiveEdge;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::node::private::InterfaceNodePrivate;
use crate::node::{Node, NodeProperties, NodeProperty, PropertyData};
use crate::pow_two::{Pow2, Pow2Relative, SizePow2};
use crate::prelude::*;
use crate::resize::ResizeFilter;
use image::Luma;
use serde::{Deserialize, Serialize};
use std::string::ToString;
use threadpool::ThreadPool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeResizePrivate {
    properties: NodeProperties,
}

const RESIZE_POLICY: &str = "resize_policy";
const RESIZE_FILTER: &str = "resize_filter";

#[typetag::serde]
impl Node for NodeResizePrivate {
    fn new_unboxed() -> Self {
        NodeResizePrivate {
            properties: NodeProperties::new(vec![
                NodeProperty::new(
                    RESIZE_POLICY,
                    PropertyData::ResizePolicy(ResizePolicy::default()),
                ),
                NodeProperty::new(
                    RESIZE_FILTER,
                    PropertyData::ResizeFilter(ResizeFilter::default()),
                ),
            ]),
        }
    }

    /// The output slots just return the input slots,
    /// because each input buffer should have a corresponding output.
    fn slots_output(&self) -> Vec<Slot> {
        self.slots_input()
    }

    fn title(&self) -> String {
        "Resize Private".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeResizePrivate {
    fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        let node = self.clone();

        let thread_pool = thread_pool.lock().unwrap();
        thread_pool.execute(move || {
            let dag_size = process_data.dag_properties.size;

            let PropertyData::ResizePolicy(resize_policy) = node.properties().get(RESIZE_POLICY)
                .unwrap_or_else(|_| panic!("expected the node to have a property named {}", RESIZE_POLICY))
                else { panic!("expected property {} to be a PropertyData::ResizePolicy", RESIZE_POLICY) };

            let PropertyData::ResizeFilter(resize_filter) = node.properties().get(RESIZE_FILTER)
                .unwrap_or_else(|_| panic!("expected the node to have a property named {}", RESIZE_FILTER))
                else { panic!("expected property {} to be a PropertyData::ResizeFilter", RESIZE_FILTER) };

            let target_size = determine_target_size(&process_data, *resize_policy, dag_size);

            // Here we get the buffers,
            // resize them,
            // and pair them with their corresponding `SlotId` to be sent back to the Engine.
            let output =
                node.properties().properties().iter().filter(|node_property| {
                    // Make sure we only get `PropertyData::Gray` `NodeProperty`s,
                    // in case there are other `NodeProperty`s on the node.
                    matches!(node_property.data, PropertyData::Gray(_))
                }).map(|node_property| {
                    let slot_data = final_slot_data(
                        node.properties(),
                        &process_data,
                        node_property.name()
                    );
                    let SlotData::SlotImage(SlotImage::Gray(buffer)) = slot_data
                        else { panic!("expected the input to be a `SlotImage::Gray`") };

                    // We want the output `SlotId` that corresponds with the current `NodeProperty`.
                    let slot_id = node
                        .slots_output()
                        .iter()
                        // We find the output slot with the same name as the input slot.
                        .find(|slot| slot.name == node_property.name)
                        .expect("all input slots should have a corresponding output slot")
                        // And get its `SlotId`.
                        .slot_id;

                    let resized_buffer = resize_buffer(&buffer, target_size, *resize_filter);
                    let slot_image = SlotData::SlotImage(SlotImage::Gray(resized_buffer));

                    (slot_id, slot_image)
                }).collect();

            process_data.send(output).unwrap()
        });
    }
}

impl NodeResizePrivate {
    /// A convenience function for adding a new slot to the NodeResize.
    ///
    /// Returns an error if there is no slot with the given name,
    /// or if the PropertyData for the slot is not a PropertyData::Gray.
    pub(crate) fn add_resize_slot(&mut self, child_node: &dyn Node, name: &str) -> Result<()> {
        // Get the PropertyData.
        let property_data = child_node
            .properties()
            .get(name)
            .map_err(|_| VismutError::InvalidSlotName(name.to_string()))?
            .clone();

        // Ensure it's a `PropertyData::Gray` since that's the only output we're interested in.
        if !matches!(property_data, PropertyData::Gray(_)) {
            Err(VismutError::InvalidPropertyVariant(property_data.clone()))?;
        }

        // Create corresponding property on the resize node.
        self.properties_mut()
            .properties
            .push(NodeProperty::new(name, property_data));

        Ok(())
    }

    /// A convenience function for inserting the NodeResize into a LiveDag.
    ///
    /// It inserts the node,
    /// creates edges between it and its child node for each resize slot,
    /// and maps its inputs between the live and regular worlds.
    ///
    /// Returns an error if the child node does not have an input slot with the same name as
    /// one of the Gray output slots on this node.
    pub(crate) fn insert_resize_node(
        self,
        live_dag: &mut LiveDag,
        creator_address: NodeAddress,
        child_node: &dyn Node,
        child_node_id: LiveNodeId,
    ) -> Result<LiveNodeId> {
        // Need to clone the node before inserting it,
        // otherwise we can't map its inputs later on.
        let node_clone = self.clone();

        // Insert the resize node and get its ID.
        let resize_node_id = live_dag.insert(creator_address, Box::new(self));

        // Get all gray output slots.
        let gray_output_slots = node_clone.slots_output();
        let gray_output_slots = gray_output_slots
            .iter()
            .filter(|slot| matches!(slot.slot_type, SlotType::Gray));

        // Go through each Gray output...
        for output_slot in gray_output_slots {
            let slot_name = &output_slot.name;

            // Get the child node's input `SlotId` with the same name.
            let child_node_input_slot = child_node
                .slot_from_name(Side::Input, slot_name)
                .map_err(|_| VismutError::InvalidSlotName(slot_name.to_string()))?
                .slot_id;

            // Add edge to `LiveDag`.
            live_dag.add_edge(LiveEdge::new(
                resize_node_id.with_slot_id(output_slot.slot_id),
                child_node_id.with_slot_id(child_node_input_slot),
            ));

            // Map the inputs to the resize node.
            live_dag.map_corresponding_name(
                creator_address,
                resize_node_id,
                Side::Input,
                &node_clone,
                slot_name,
            );
        }

        Ok(resize_node_id)
    }
}

fn determine_target_size(
    process_data: &ProcessData,
    resize_policy: ResizePolicy,
    dag_size: SizePow2,
) -> SizePow2 {
    match resize_policy {
        ResizePolicy::RelativeToInput(pow_2_relative) => {
            let slot_data = process_data.slot_data_package.first();

            let first_input_size = if let Some((_, SlotData::SlotImage(slot_image))) = slot_data {
                slot_image.size()
            } else {
                SizePow2::default()
            };

            first_input_size.saturating_add(pow_2_relative)
        }
        ResizePolicy::RelativeToGraph(pow_2_relative) => dag_size.saturating_add(pow_2_relative),
        ResizePolicy::Absolute(size_pow_2) => size_pow_2,
    }
}

fn resize_buffer(
    buffer: &Arc<Buffer>,
    target_size: SizePow2,
    resize_filter: ResizeFilter,
) -> Arc<Buffer> {
    let (width, height) = (
        Pow2::log_2(buffer.width() as usize).expect("width was not a power of two"),
        Pow2::log_2(buffer.height() as usize).expect("height was not a power of two"),
    );
    let input_size = SizePow2::new(width, height);
    let (width_difference, height_difference) = target_size.difference(input_size);

    match resize_filter {
        ResizeFilter::Nearest => {
            let resized_horizontal = resize_nearest_horizontal(buffer, width_difference);
            resize_nearest_vertical(&resized_horizontal, height_difference)
        }
        ResizeFilter::Bilinear => {
            let resized_horizontal = resize_linear_horizontal(buffer, width_difference);
            resize_linear_vertical(&resized_horizontal, height_difference)
        }
    }
}

fn resize_nearest_horizontal(buffer: &Arc<Buffer>, difference: Pow2Relative) -> Arc<Buffer> {
    let (width, height) = buffer.dimensions();
    let power_difference = difference.inner();
    let difference_abs = 2_u32.pow(power_difference.unsigned_abs() as u32);

    match power_difference {
        i if i > 0 => {
            let resized_buffer = Buffer::from_fn(width * difference_abs, height, |x, y| {
                let x = x / difference_abs;
                *buffer.get_pixel(x, y)
            });

            Arc::new(resized_buffer)
        }
        i if i < 0 => {
            let resized_buffer = Buffer::from_fn(width / difference_abs, height, |x, y| {
                let x = x * difference_abs;
                *buffer.get_pixel(x, y)
            });

            Arc::new(resized_buffer)
        }
        _ => Arc::clone(buffer),
    }
}

fn resize_nearest_vertical(buffer: &Arc<Buffer>, difference: Pow2Relative) -> Arc<Buffer> {
    let (width, height) = buffer.dimensions();
    let power_difference = difference.inner();
    let difference_abs = 2_u32.pow(power_difference.unsigned_abs() as u32);

    match power_difference {
        i if i > 0 => {
            let height = height * difference_abs;
            let resized_buffer = Buffer::from_fn(width, height, |x, y| {
                let y = y / difference_abs;
                *buffer.get_pixel(x, y)
            });

            Arc::new(resized_buffer)
        }
        i if i < 0 => {
            let height = height / difference_abs;
            let resized_buffer = Buffer::from_fn(width, height, |x, y| {
                let y = y * difference_abs;
                *buffer.get_pixel(x, y)
            });

            Arc::new(resized_buffer)
        }
        _ => Arc::clone(buffer),
    }
}

fn resize_linear_horizontal(buffer: &Arc<Buffer>, difference: Pow2Relative) -> Arc<Buffer> {
    let (source_width, source_height) = buffer.dimensions();
    let power_difference = difference.inner();
    let difference_abs = 2_u32.pow(power_difference.unsigned_abs() as u32);

    match power_difference {
        i if i > 0 => {
            let new_width = source_width * difference_abs;
            let resized_buffer = Buffer::from_fn(new_width, source_height, |x, y| {
                let x_source = x / difference_abs;
                let x_source_next = (x_source + 1) % source_width;

                let sample_a = buffer.get_pixel(x_source, y).0[0];
                let sample_b = buffer.get_pixel(x_source_next, y).0[0];

                let interpolation_amount = (x % difference_abs) as f32 / difference_abs as f32;

                let final_value = ((sample_b - sample_a) * interpolation_amount) + sample_a;
                Luma::from([final_value])
            });

            Arc::new(resized_buffer)
        }
        i if i < 0 => {
            let new_width = source_width / difference_abs;
            let resized_buffer = Buffer::from_fn(new_width, source_height, |x, y| {
                let x_source = x * difference_abs;

                let mut result = 0.0_f32;
                for i in 0..difference_abs {
                    result += buffer.get_pixel(x_source + i, y).0[0];
                }
                result /= difference_abs as f32;

                Luma::from([result])
            });

            Arc::new(resized_buffer)
        }
        _ => Arc::clone(buffer),
    }
}

fn resize_linear_vertical(buffer: &Arc<Buffer>, difference: Pow2Relative) -> Arc<Buffer> {
    let (source_width, source_height) = buffer.dimensions();
    let power_difference = difference.inner();
    let difference_abs = 2_u32.pow(power_difference.unsigned_abs() as u32);

    match power_difference {
        i if i > 0 => {
            let new_height = source_height * difference_abs;
            let resized_buffer = Buffer::from_fn(source_width, new_height, |x, y| {
                let y_source = y / difference_abs;
                let y_source_next = (y_source + 1) % source_height;

                let sample_a = buffer.get_pixel(x, y_source).0[0];
                let sample_b = buffer.get_pixel(x, y_source_next).0[0];

                let interpolation_amount = (y % difference_abs) as f32 / difference_abs as f32;

                let final_value = ((sample_b - sample_a) * interpolation_amount) + sample_a;
                Luma::from([final_value])
            });

            Arc::new(resized_buffer)
        }
        i if i < 0 => {
            let new_height = source_height / difference_abs;
            let resized_buffer = Buffer::from_fn(source_width, new_height, |x, y| {
                let y_source = y * difference_abs;

                let mut result = 0.0_f32;
                for i in 0..difference_abs {
                    result += buffer.get_pixel(x, y_source + i).0[0];
                }
                result /= difference_abs as f32;

                Luma::from([result])
            });

            Arc::new(resized_buffer)
        }
        _ => Arc::clone(buffer),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::live::address::LiveNodeId;
    use crate::live::node::FinishedProcessing;
    use crate::pow_two::MAX_POW;
    use std::sync::mpsc;

    // Nearest neighbor filtering.
    #[test]
    fn nearest_horizontal_upscale() {
        let buffer = Buffer::from_raw(
            2,
            2,
            vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(2);

        let buffer = resize_nearest_horizontal(&buffer, difference);

        assert_eq!(
            buffer.as_raw(),
            &vec![
                1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, //
                3.0, 3.0, 3.0, 3.0, 4.0, 4.0, 4.0, 4.0,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn nearest_horizontal_downscale() {
        let buffer = Buffer::from_raw(
            8,
            2,
            vec![
                1.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0, //
                3.0, 0.0, 0.0, 0.0, 4.0, 0.0, 0.0, 0.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(-2);

        let buffer = resize_nearest_horizontal(&buffer, difference);

        assert_eq!(
            buffer.as_raw(),
            &vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn nearest_vertical_upscale() {
        let buffer = Buffer::from_raw(
            2,
            2,
            vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(2);

        let buffer = resize_nearest_vertical(&buffer, difference);

        assert_eq!(
            buffer.as_raw(),
            &vec![
                1.0, 2.0, //
                1.0, 2.0, //
                1.0, 2.0, //
                1.0, 2.0, //
                3.0, 4.0, //
                3.0, 4.0, //
                3.0, 4.0, //
                3.0, 4.0,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn nearest_vertical_downscale() {
        let buffer = Buffer::from_raw(
            2,
            8,
            vec![
                1.0, 2.0, //
                0.0, 0.0, //
                0.0, 0.0, //
                0.0, 0.0, //
                3.0, 4.0, //
                0.0, 0.0, //
                0.0, 0.0, //
                0.0, 0.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(-2);

        let buffer = resize_nearest_vertical(&buffer, difference);

        assert_eq!(
            buffer.as_raw(),
            &vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
            "buffer did not match expectations"
        );
    }

    // Linear filtering
    #[test]
    fn linear_horizontal_upscale() {
        let buffer = Buffer::from_raw(
            2,
            2,
            vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(2);

        let buffer = resize_linear_horizontal(&buffer, difference);

        // Multiply by 4 and round to ints to simplify comparisons.
        let buffer_int = buffer
            .pixels()
            .map(|pixel| ((pixel.0[0] * 4.0) + 0.5) as usize)
            .collect::<Vec<_>>();

        assert_eq!(
            buffer_int,
            vec![
                4, 5, 6, 7, 8, 7, 6, 5, //
                12, 13, 14, 15, 16, 15, 14, 13,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn linear_horizontal_downscale() {
        let buffer = Buffer::from_raw(
            8,
            2,
            vec![
                1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, //
                2.0, 1.0, 1.0, 0.0, 4.0, 0.0, 4.0, 0.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(-2);

        let buffer = resize_linear_horizontal(&buffer, difference);

        // Multiply by 4 and round to ints to simplify comparisons.
        let buffer_int = buffer
            .pixels()
            .map(|pixel| ((pixel.0[0] * 4.0) + 0.5) as usize)
            .collect::<Vec<_>>();

        assert_eq!(
            buffer_int,
            vec![
                4, 2, //
                4, 8,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn linear_vertical_upscale() {
        let buffer = Buffer::from_raw(
            2,
            2,
            vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(2);

        let buffer = resize_linear_vertical(&buffer, difference);

        // Multiply by 4 and round to ints to simplify comparisons.
        let buffer_int = buffer
            .pixels()
            .map(|pixel| ((pixel.0[0] * 4.0) + 0.5) as usize)
            .collect::<Vec<_>>();

        assert_eq!(
            buffer_int,
            vec![
                4, 8, //
                6, 10, //
                8, 12, //
                10, 14, //
                12, 16, //
                10, 14, //
                8, 12, //
                6, 10,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn linear_vertical_downscale() {
        let buffer = Buffer::from_raw(
            2,
            8,
            vec![
                1.0, 1.0, //
                1.0, 1.0, //
                1.0, 1.0, //
                0.0, 0.0, //
                2.0, 1.0, //
                1.0, 0.0, //
                4.0, 0.0, //
                4.0, 0.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(-2);

        let buffer = resize_linear_vertical(&buffer, difference);

        // Multiply by 4 and round to ints to simplify comparisons.
        let buffer_int = buffer
            .pixels()
            .map(|pixel| ((pixel.0[0] * 4.0) + 0.5) as usize)
            .collect::<Vec<_>>();

        assert_eq!(
            buffer_int,
            vec![
                3, 3, //
                11, 1,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn determine_target_size() {
        let (sender, _) = mpsc::channel();
        let slot_image = SlotImage::gray_from_value(SizePow2::new(Pow2::new(2), Pow2::new(4)), 0.0)
            .expect("could not create `SlotImage`");
        let process_data = ProcessData {
            sender,
            live_node_id: LiveNodeId(0),
            slot_data_package: vec![(SlotId(0), SlotData::SlotImage(slot_image))],
            dag_properties: Default::default(),
        };
        let parent_size = SizePow2::default();

        let resize_policy = ResizePolicy::RelativeToInput(Pow2Relative::new(2));
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        let compare_size = SizePow2::new(Pow2::new(4), Pow2::new(6));
        assert_eq!(
            target_size, compare_size,
            "determined size did not match expectation"
        );

        let resize_policy = ResizePolicy::RelativeToInput(Pow2Relative::new(-10));
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        let compare_size = SizePow2::new(Pow2::new(0), Pow2::new(2));
        assert_eq!(
            target_size, compare_size,
            "determined size did not match expectation"
        );

        let resize_policy = ResizePolicy::RelativeToInput(Pow2Relative::new(MAX_POW as i8));
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        let compare_size = SizePow2::new(Pow2::new(MAX_POW - 2), Pow2::new(MAX_POW));
        assert_eq!(
            target_size, compare_size,
            "determined size did not match expectation"
        );

        let absolute_size = SizePow2::new(Pow2::new(8), Pow2::new(10));
        let resize_policy = ResizePolicy::Absolute(absolute_size);
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        assert_eq!(
            target_size, absolute_size,
            "determined size did not match expectation"
        );

        let parent_size = SizePow2::new(Pow2::new(7), Pow2::new(10));
        let resize_policy = ResizePolicy::RelativeToGraph(Pow2Relative::new(-1));
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        let compare_size = SizePow2::new(Pow2::new(6), Pow2::new(9));
        assert_eq!(
            target_size, compare_size,
            "determined size did not match expectation"
        );
    }

    /// Tests the process function of the node to see if it properly sends its message.
    #[test]
    fn process() {
        const INPUT_NAME: &str = "input_0";

        let (sender, receiver) = mpsc::channel();
        let expected_value = 0.0;
        let slot_image =
            SlotImage::gray_from_value(SizePow2::new(Pow2::new(2), Pow2::new(4)), expected_value)
                .expect("could not create `SlotImage`");
        let live_node_id = LiveNodeId(0);
        let process_data = ProcessData {
            sender,
            live_node_id,
            slot_data_package: vec![(SlotId(0), SlotData::SlotImage(slot_image))],
            dag_properties: Default::default(),
        };

        let thread_pool = Mutex::new(ThreadPool::new(1));

        let expected_size = SizePow2::new(Pow2::new(1), Pow2::new(1));
        let resize_policy = PropertyData::ResizePolicy(ResizePolicy::Absolute(expected_size));

        // Create and set up node.
        let mut node_resize = NodeResizePrivate::new();
        *node_resize.properties.get_mut(RESIZE_POLICY).unwrap() = resize_policy;
        node_resize
            .properties_mut()
            .properties
            .push(NodeProperty::new(
                INPUT_NAME,
                PropertyData::Gray(expected_value),
            ));

        node_resize.process(&thread_pool, process_data);

        let message = receiver.recv().expect("could not receive a message");

        let expected_slot_id = node_resize
            .slots_output()
            .iter()
            .find(|slot| slot.name == *INPUT_NAME)
            .unwrap()
            .slot_id;
        let expected_slot_image = SlotImage::gray_from_value(expected_size, expected_value)
            .expect("could not create expected image");
        let expected_message = FinishedProcessing {
            live_node_id,
            slot_data_packages: vec![(expected_slot_id, SlotData::SlotImage(expected_slot_image))],
        };

        assert_eq!(message, expected_message);
    }
}
