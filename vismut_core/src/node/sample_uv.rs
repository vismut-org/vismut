use crate::address::{NodeAddress, SlotId};
use crate::live::dag::LiveDag;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::node::private::InterfaceNodePrivate;
use crate::node::resize_private::NodeResizePrivate;
use crate::node::{
    final_slot_data, sample_image, Node, NodeProperties, NodeProperty, PropertyData,
};
use crate::prelude::*;
use image::Luma;
use serde::{Deserialize, Serialize};
use std::string::ToString;
use std::sync::{Arc, Mutex};
use threadpool::ThreadPool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeSampleUv {
    properties: NodeProperties,
}

const RESIZE_POLICY: &str = "resize_policy";
const RESIZE_FILTER: &str = "resize_filter";
const INPUT: &str = "input";
const U: &str = "u";
const V: &str = "v";
const OUTPUT: &str = "output";

#[typetag::serde]
impl Node for NodeSampleUv {
    fn new_unboxed() -> Self {
        NodeSampleUv {
            properties: NodeProperties::new(vec![
                NodeProperty::new(
                    RESIZE_POLICY,
                    PropertyData::ResizePolicy(ResizePolicy::default()),
                ),
                NodeProperty::new(
                    RESIZE_FILTER,
                    PropertyData::ResizeFilter(ResizeFilter::default()),
                ),
                NodeProperty::new(INPUT, PropertyData::Gray(0.0)),
                NodeProperty::new(U, PropertyData::Gray(0.0)),
                NodeProperty::new(V, PropertyData::Gray(0.0)),
            ]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![Slot::new(SlotType::Gray, SlotId(0), OUTPUT)]
    }

    fn title(&self) -> String {
        "Sample UV".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeSampleUv {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        let sample_uv_node_id = live_dag.insert(creator_address, Box::new(self.clone()));

        // Create the resize node.
        let mut node_resize = NodeResizePrivate::new();

        // Configure the resize node.
        let resize_policy = self
            .properties()
            .get(RESIZE_POLICY)
            .expect("expected a `PropertyData::ResizePolicy`")
            .clone();
        *node_resize
            .properties_mut()
            .get_mut(RESIZE_POLICY)
            .expect("expected a `PropertyData::ResizePolicy`") = resize_policy;

        let resize_filter = self
            .properties()
            .get(RESIZE_FILTER)
            .expect("expected a `PropertyData::ResizeFilter`")
            .clone();
        *node_resize
            .properties_mut()
            .get_mut(RESIZE_FILTER)
            .expect("expected a `PropertyData::ResizeFilter`") = resize_filter;

        node_resize.add_resize_slot(self, INPUT).unwrap();
        node_resize.add_resize_slot(self, U).unwrap();
        node_resize.add_resize_slot(self, V).unwrap();

        // Insert the resize node.
        node_resize
            .insert_resize_node(live_dag, creator_address, self, sample_uv_node_id)
            .unwrap();

        // Map output.
        live_dag.map_corresponding_name(
            creator_address,
            sample_uv_node_id,
            Side::Output,
            self,
            OUTPUT,
        );
    }

    fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        // Clone the node's properties so we can send it to the thread.
        let properties = self.properties().clone();

        let thread_pool = thread_pool.lock().unwrap();
        thread_pool.execute(move || {
            // Get the data from the slots.
            let SlotData::SlotImage(SlotImage::Gray(input)) =
                final_slot_data(&properties, &process_data, INPUT)
                else { panic!("expected input image to exist") };
            let SlotData::SlotImage(SlotImage::Gray(u)) =
                final_slot_data(&properties, &process_data, U)
                else { panic!("expected U image to exist") };
            let SlotData::SlotImage(SlotImage::Gray(v)) =
                final_slot_data(&properties, &process_data, V)
                else { panic!("expected V image to exist") };

            // Create the new image.
            let image_buffer = {
                Buffer::from_fn(input.width(), input.height(), |x, y| {
                    let u = u.get_pixel(x, y).0[0] * input.width() as f32;
                    let v = v.get_pixel(x, y).0[0] * input.width() as f32;

                    Luma([sample_image(&input, u, v, true, true)])
                })
            };

            // Package it up in the appropriate types.
            let slot_data = SlotData::SlotImage(SlotImage::Gray(Arc::new(image_buffer)));

            // Send it.
            process_data.send(vec![(SlotId(0), slot_data)]).unwrap();
        });
    }
}
