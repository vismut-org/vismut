use super::prelude::*;

use crate::live::dag::LiveDag;

use crate::prelude::*;

/// This node exists to allow its non-private sibling `NodeOutputRgba` to *not* have an output slot.
///
/// It would be confusing for the user to see an output slot on an output node,
/// but the slot is necessary to be able to store the actual output data.
/// So this private node exists to be able to have that output slot,
/// and that way we avoid having to a special case for this situation.
/// The public node simply gets "activated" into this node.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeOutputRgbaPrivate {
    properties: NodeProperties,
}

const INPUT: &str = "input";
const OUTPUT: &str = "output";

#[typetag::serde]
impl Node for NodeOutputRgbaPrivate {
    fn new_unboxed() -> Self {
        NodeOutputRgbaPrivate {
            properties: NodeProperties::new(vec![NodeProperty::new(
                INPUT,
                PropertyData::Rgba([0.0, 0.0, 0.0, 1.0]),
            )]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![Slot::new(SlotType::Rgba, SlotId(0), OUTPUT)]
    }

    fn title(&self) -> String {
        "Output RGBA Private".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeOutputRgbaPrivate {
    fn activate(&self, _live_dag: &mut LiveDag, _creator_address: NodeAddress) {
        panic!("a `NodeOutputRgbaPrivate` node should never be activated")
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        // Get the `SlotData` on the input slot.
        let slot_data = final_slot_data(self.properties(), &process_data, INPUT);

        // Send it.
        process_data.send(vec![(SlotId(0), slot_data)]).unwrap();
    }
}
