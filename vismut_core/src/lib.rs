extern crate core;

pub mod address;
mod blueprint;
mod dag;
pub mod edge;
mod engine;
pub mod error;
mod live;
pub mod node;
pub mod pow_two;
pub mod resize;
pub mod slot;

#[allow(missing_docs)]
pub mod prelude {
    #[doc(hidden)]
    pub use crate::address::{
        DagId, NodeAddress, NodeId, Side, SlotAddress, SlotAddressSide, SlotId,
    };
    #[doc(hidden)]
    pub use crate::live::slot_data::{Buffer, SlotData};
    #[doc(hidden)]
    pub use crate::node::MathOperation;
    #[doc(hidden)]
    pub use crate::resize::{Resize, ResizeFilter, ResizePolicy};
    #[doc(hidden)]
    pub use crate::{
        dag::{Dag, SerializeableDagV0, SerializedPosition},
        edge::Edge,
        engine::{Engine, NodeState, VismutPixel},
        // nodes::{
        //     GrayscaleInfoNode, GrayscaleNode, ImageNode, MergeRgbaNode, OutputRgbaNode,
        //     SplitRgbaNode,
        // },
        node::float::NodeFloat,
        slot::{Slot, SlotType},
    };
}
