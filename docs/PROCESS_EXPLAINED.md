# Vismut Contribution Process Explained

This document explains the **Vismut Contribution Process** (VCP).
It lists the goals of the process,
how each part of it aims to reach those goals,
and gives examples.

This document doesn't contain the contents of the actual VCP,
so to get the context you need to understand the explanations,
it's probably a good idea to have the VCP open while reading this document.
The VCP can be found in [PROCESS.md](PROCESS.md).

The VCP is inspired by the [C4 process](https://rfc.zeromq.org/spec/42/).

## The Goal

The goal of the VCP is to **create software that is as useful as possible for as long as possible**.
This goal is achieved by:

- **Making the project less reliant on specific people**,
to avoid bottlenecks and failure points
- **Building a large community of people**,
to benefit from "the wisdom of crowds" to build better software
- **Ensuring people are solving real problems**,
to keep a focus on usefulness

The rest of this document goes through different aspects of the VCP,
and how they help achieve the goal through the three points listed above.

## Anyone Can Easily Contribute

Anyone can join the community and submit improvements,
and it's also relatively easy to get your contributions accepted.
A merge request usually gets merged within a day if it passes a few criteria such as "solving a problem" and "passing continuous integration (CI)".

Continuous integration (CI) won't pass if `cargo test` doesn't pass,
`cargo clippy` outputs any warnings,
or the code isn't formatted with `cargo fmt`.

There are several benefits to making it easy to contribute:

- Helps build a large community by making it more fun and rewarding to submit improvements
- A larger number of contributors creates better software through "the wisdom of crowds"
- Reduces overhead for maintainers by making it less work to accept contributions
- Enables the software to evolve faster

But what if someone submits a less than perfect solution to a problem?
What if the code creates performance issues?
What if there's a bug?
Since it's easy to submit changes,
issues like these can be easily fixed with more merge requests.
If a merge request creates more problems than it solves,
someone can submit a merge request that reverts the changes.

The VCP tries to get as many people together as possible,
and let them work together through a lightweight and well documented process to minimize the effort required to add value to the project.

## Roles

If a project relies on few people,
there's a risk the project might just stop if something happens to those people.
If progress relies on them to do something to keep things moving,
they might also become a bottleneck.
The VCP uses roles to avoid these issues,
making the project less reliant on specific people,
and helping build a large community of people.

Here are the three roles in the VCP,
and a very short description of each:

- **Maintainer**,
  enforces the [Vismut Contribution Process](PROCESS.md)
- **Moderator**,
  enforces the [Vismut Code of Conduct](../CODE_OF_CONDUCT.md)
- **Administrator**,
  gives roles and does all the other things

Each role does a few specific things.
This means you only need some specific knowledge and interest to perform a role,
increasing the number of people who can potentially fill each role.
A person can have more than one role,
so you can be for instance both maintainer and moderator if your interests and skills allow for it.

The tasks of each role is documented.
This increases the chance of someone being able to do a good job in a role.
The documentation also makes it easier to talk about and make changes to the roles.

The administrator role has the task of inviting fitting people to each role,
ensuring that there are enough people to handle a growing community.
This means the community can grow more freely without running into issues.
The administrators also do "all the other things".
So if something needs to be done,
and none of the other roles have it in their description,
it should probably be done by an administrator.

There are fewer administrators than moderators and maintainers,
because adding more administrators won't necessarily help the project grow faster.
Promoting people to administrator too easily might cause issues since the power can more easily be abused.
It's very important the right people get invited to this role to ensure people feel safe in the project.

## Focusing on Solving Problems

In the VCP,
both the title of an issue in the issue tracker and the title of a merge request need to describe a problem.
There are no exceptions to this rule.

The idea is that if a proposed change can't be expressed as a problem it's likely not useful.
By having everything conform to this pattern,
maintainers don't need to decide what does and doesn't need to be expressed as a problem.
This makes the maintainer's job easier,
and the process more robust.

It's not a big deal if someone submits an issue or merge request that doesn't describe a problem.
Maintainers help people make their issues and merge requests conform to the requirement when possible.
A maintainer might for instance rewrite the title as a problem description,
then they would ask the submitter to change it to another problem description if they disagree with the change.
The maintainer would also point to the VCP as the reason for the changes.

We want to make it as easy as possible for people to contribute.
We don't expect everyone to have read and understood the VCP before submitting an issue or merge request.
Instead of asking people to conform to the requirements,
we show them how.
This is faster and less work for contributors and maintainers,
and it very clearly teaches the contributor how things work,
without them having to read the VCP and this document.

We of course encourage people to understand the VCP and the code of conduct,
but it's not a requirement that they do so.
They are bound by them regardless.

### Issues

The title of an issue needs to describe a problem,
but the description of an issue can be whatever the submitter thinks is appropriate.
People tend to prioritise working on important issues,
so if you're able to make a good case for why the issue is important to fix,
you increase the chances of it getting fixed.

Only allowing issues that state a problem in the title means that things like feature requests,
ideas,
and suggestions aren't allowed.
But in many cases,
those things can be described as problems instead.
For example,
here is an issue that would **not** be accepted:

**Title**:
`Add thumbnail previews when hovering inputs`

**Description**:
`It would be faster if I can see what's connected to a socket without having to find the actual node.`

Unless this issue can be rephrased into a description of a problem (maintainers help with this),
it will be closed.
This particular issue could be rephrased like this:

**Title**:
`It can take time to find the thumbnail of a connected input`

**Description**:
`You have to manually follow the lines to the other nodes, which might be far away. Suggested solution:
Show the thumbnail of the connected node when hovering a socket.`

Now it's expressed as a problem with a suggested solution instead,
and it will be able to stay in the issue tracker for people to discuss and solve.

If someone submits a feature request,
the only way to satisfy that feature request is to implement the specific feature.
If the feature is instead phrased as a problem,
that opens up for many more ways to solve the underlying problem.

### Merge Requests

For merge requests,
the description needs to start by saying how the merge requests solves the problem in the title.
After describing the solution,
the rest of the description can be whatever the submitter thinks is appropriate,
or nothing at all.

Here is an example of a title and description for a merge request that will **not** get merged unless it's changed:

**Title**:
`Updated input library to the latest version`

**Description**:
`From version 1.2 to 1.3.`

Since it does not state a problem in the title,
it needs to be changed before it can be accepted.
Changing it to this means it **might** get merged:

**Title**:
`The compiled binary is larger than it needs to be`

**Description**:
`Solution: Updated input library from version 1.2 to 1.3. We use a different version of our input library than another dependency we use does. This means we have both versions of the library compiled into our final binary, increasing the size of the program without any benefit.`
