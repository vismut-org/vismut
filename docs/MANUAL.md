# Vismut Manual for v0.5

Vismut currently has one use case implemented: manually packing image channels.

Notable features that are not yet implemented:

- Copy/paste & duplicating nodes
- A node to perform math operations on images to blend them
- A node to generate noises

## Community

Join the [Vismut Zulip](https://vismut.zulipchat.com) if you have any questions
or want to talk to other users, welcome! 😃

## Controls

- `Middle click` and drag: Move view
- `Left click`: select a node or connection
- `Left click` and drag on empty space: Box select
- `Left click` and drag on node: Move selected nodes, or the hovered node if it
  is not selected
- `Left click` and drag on slot: Start creating a connection, drop it on another
  slot to connect them
- `Ctrl` `Left click` and drag on a slot: Grab everything that's
  connected to the slot, drop it on empty space to disconnect them
- `Delete` or `X`: remove selection
- `G`: Grab and move selected nodes
- `Escape`: Cancel active tool

All other controls are shown alongside each function in the dropdown menus in
the menu bar along the top.

## Node Properties

The active node's properties can be edited in the properties panel on the right side of the screen. Click on a node to
make it active, indicated by an aqua border. Some node
types have special properties, but all node types have the following:

- `Resize Policy` - how the size of the node's outputs are decided.
- `Resize Filter` - if resizing should use nearest neighbour or triangle (also known as
  "linear") filtering when resizing.

Each node's special properties are detailed under the "Node Types" heading.

## Packing Channels

Packing is when you take channels from one or more images and put them together into a new image.

1. Create one or more `Image` nodes to import your starting images, all nodes
   are created from the **Add Node** menu at the top.
1. Plug each `Image` node into a `Separate` node to expose each individual
   channel of the image.
1. Create one `Combine` node for each output image you want.
1. Plug the outputs from the `Separate` node(s) into the `Combine` node(s) to
   pack them how you want. The channels are laid out from top to bottom in this
   order: `Red`, `Green`, `Blue`,
   `Alpha`.
1. Export the image(s) as instructed below.

## Exporting images

- If you plan on exporting a single image once: Click it to make it active,
  so it has an aqua border, and press `File`->`Export Active…`.
- If you want to export several images, or the same image several times:
    1. Create an `Output` node for each image you want to export and plug them
       in.
    2. Set each `Output` node's name by clicking them and setting the name in
       the properties panel on the right.
    3. Press `File`➤`Export` and select a folder to export to.

Images are currently always exported as PNG. More export options will be added in the future.

## Node Types

### Shared Properties

These properties are used by several node types.

- Size From: Decides how the node's size is determined. The size is always a power of two.
    - Input: Calculated relative to the size of the first input from the top.
    - Absolute: A specific width and height.
- Resize Filter: How the color of each pixel is chosen when resizing.
    - Nearest: Take the closest pixel from the source image, causes a pixelated look when scaling up, and a noisy look
      when scaling down.
    - Bilinear: Linearly interpolates when scaling up, and averages the colors when scaling down. Creates a smoother
      look.

### Image

Imports an image.

- Path: The path the image was loaded from, can not be changed.

> **Note**: The image does not automatically update when the source image is changed.

> **Tip**:The file window that lets you pick the image actually allows you to pick several images at once.

### Split

Splits an RGBA input into 4 grayscale outputs.

### Merge

Merges 4 grayscale inputs into an RGBA output. R, G, and B defaults to black if
they have no input, and alpha defaults to white.

### Output

Exports an RGBA input to a PNG image on disk when `File`->`Export` is pressed.
Images with a single channel can not be exported yet.

**Properties**

- Name: The name of the created PNG image, the ".png" at the end is added
  automatically on export.

### Grayscale

Outputs a grayscale channel containing a single pixel with the given value.

**Properties**

- Value: The value of the output pixel.

### UV Coordinates

Takes the size of an image as powers of two integers.

Returns a grayscale U and V coordinate images of the given size.
These are just vertical and horizontal gradients.

### Sample UV

Takes an image, a U coordinate and a V coordinate.
The image is sampled on each pixel using the U and V coordinates.

### Float

Creates a decimal number.

### Int

Creates a whole number.

### Grayscale Info

Extracts information from a grayscale buffer.
It outputs the width and height of the buffer as a power of two.

### Math Grayscale

Takes two grayscale buffers,
does math on each pixel,
and returns a single grayscale buffer containing the result.

Some math operations only use the first input.

**Operations**

- **Add**: Does `input_1 + input_2`
- **Subtract**: Does `input_1 - input_2`
- **Multiply**: Does `input_1 * input_2`
- **Divide**: Does `input_1 / input_2`
- **Power**: Does `input_1 ^ input_2`
- **Square Root**: Returns the square root of input_1
- **Sin**: Returns the sine of input_1
- **Cos**: Returns the cosine of input_1

### Distribute Grayscale

Takes an input image and some parameters,
duplicates and distributes the input image based on the parameters.

This is a building block for generating noises and patterns.

It is hard to use right now,
because there are no labels on the input slots,
and there are no parameters on the node.

Here is what each input slot does:

1: The grayscale image to distribute
2 & 3: The width and height of the output as a power of two
4 & 5: The number of images on the X and Y axis
6 & 7: The scale of the image on X and Y
8 & 9: The random offset of each image on the X and Y axis
